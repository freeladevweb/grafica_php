-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 09-Set-2020 às 17:15
-- Versão do servidor: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_grafica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('n9900vhhli8togr8enul5es02631l9fl', '::1', 1599303237, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393330333233373b),
('uh3njk5kjlvc411hr1cbn2i0kilaoej8', '::1', 1599309506, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393330393530363b),
('g9r8po5nian440ptlcgh7kgiemjhb41o', '::1', 1599309998, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393330393939383b),
('gv5p0jb0pr2eus5b1ca7krsqflv2tu30', '::1', 1599310419, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331303431393b),
('g467gegmv3jlm8ka35h4fdhtu87cdgp2', '::1', 1599310761, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331303736313b),
('m1d2jgbi1p3dnoqq9hqg4cjq71djpp6g', '::1', 1599311505, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331313530353b),
('lpdhn0t9558alsoji833hcv16agk12e6', '::1', 1599312121, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331323132313b),
('he0osj0kqnmtg5dq56eljmh7a2dvb69q', '::1', 1599312507, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331323530373b),
('gboocce2gn9atmd21hbgvvn20f16flql', '::1', 1599313975, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331333937353b),
('kcfqcr3c63pv0vmdg3c0hmfikse6or0a', '::1', 1599314332, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331343333323b),
('vnj4f0e9mi6f61ethr37guh9kbj6m1p9', '::1', 1599315090, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331353039303b),
('4qpdkuccsm0okj343l5b3dhjp44jh5p8', '::1', 1599315419, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331353431393b),
('963vu5e2fkjsobiouk05pa39rqr6e1rj', '::1', 1599315726, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331353732363b),
('98a2uk9g03uaq0ji0vrk9m3172p01qs6', '::1', 1599316519, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331363531393b),
('aaa2d27jbbakj0oq607dgn2kmkhovrfr', '::1', 1599317333, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331373333333b),
('eh26h2hvic7n6av2s5rn2f48t82cvmq4', '::1', 1599318394, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393331383339343b),
('anobturtsthort82coogn3ma2pf7am0p', '::1', 1599320312, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332303331323b),
('73720lqiebn1nlfna2n3qkemqjgkvvdl', '::1', 1599320821, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332303832313b),
('st222nrklk31bosu7qemvnoacdtedgra', '::1', 1599321150, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332313135303b),
('ngunroilehnbnsdoi35qafi2cvjrfm7k', '::1', 1599321647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332313634373b),
('4uvtberlvc3k6ppbesp98nd3emg9mc4m', '::1', 1599322015, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332323031353b),
('uf6i0icell7vcdcd9f3vpcn2gf37d07p', '::1', 1599322411, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332323431313b),
('o13v66lh42abvr0g1leqsi057fvu7c09', '::1', 1599323067, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332333036373b),
('qgr86n8h3jr34vkccgjmk0jgcnbgl63n', '::1', 1599323431, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332333433313b),
('a0i6drnuqrlrcdc3able1k7o40dka65h', '::1', 1599323795, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332333739353b),
('t2lrjccaeatjssa6njba68gccfcndd6n', '::1', 1599324186, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332343138363b),
('s3pfb46nso3tqs2sctev79vpmi7o7t00', '::1', 1599325084, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332353038343b),
('dj3p9qdgmon6pm52t3jpd0r8732forac', '::1', 1599325749, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332353734393b),
('v3hbeiughhf05grqummru64suakqdehi', '::1', 1599326519, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332363531393b),
('74q082nnv1mcg7uqv2mbbf9h2cfb37cj', '::1', 1599326831, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332363833313b),
('e3ad2dfs7gpgrljc0hlu9hk7f7a0j4i9', '::1', 1599327164, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332373136343b),
('qftjjcv1680v2hq2mer7v725mmv74d4n', '::1', 1599327506, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332373530363b),
('cmd4ov7tkj98ouka2k7r4v3s6jsl0o7i', '::1', 1599327881, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332373838313b),
('ei55iuhvbpv3ksikgqjll9948c26a15v', '::1', 1599328191, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332383139313b),
('tkls50rvar3833if8l86cqlv8empieo6', '::1', 1599328679, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332383637393b),
('flapmdc225suj45v3qkbrht6gqqdj6dk', '::1', 1599329021, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332393032313b),
('6r3g5ki9cacdkvv4v03k1cno756pl1a9', '::1', 1599329352, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332393335323b),
('8db6sdomdlr7rbn3fqp2cqv03sclu7rq', '::1', 1599329659, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393332393635393b),
('tade45bkni1p27nnr7s6c6iicc88miij', '::1', 1599330122, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393333303132323b),
('7g051s4o8snmasvp2henu7f83bsvrgc4', '::1', 1599330726, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393333303732363b),
('tpse378ltc7rdduec8ipfvbsv1gjaekp', '::1', 1599330755, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393333303732363b),
('ra5u81795om439a4aalsctblgbvnvf0o', '::1', 1599397160, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393339373136303b),
('1g8hgrehnp5hipmvsfpdm2hned2r8sp0', '::1', 1599397464, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393339373436343b),
('j7mac4o65u26b0vtqvfqiqva94g73apj', '::1', 1599397778, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393339373737383b),
('vltuacm6ril4ml5ssl3nidokigdrirbb', '::1', 1599398393, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393339383339333b),
('v61cdkbc0qmeoke43ibfjchp70pkfflh', '::1', 1599399077, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393339393037373b),
('d5qr34pdlqb3ueffhqsrep488kak0upp', '::1', 1599399077, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393339393037373b),
('7i9hhk4trflapn9hu2otire4n4j3uhhu', '::1', 1599577917, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393537373931373b),
('h8fgb90hmborr416q6fngrqspgtooact', '::1', 1599579171, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393537393137313b),
('0265uk3i9532c39fjheocco1i8lm6lju', '::1', 1599579746, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393537393734363b),
('1664siinhh4obvb6e2dkg7uh66fd62kh', '::1', 1599580068, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393538303036383b),
('o8u3qqbfnrb2kvakj18ukh6gdc1j66po', '::1', 1599580280, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393538303036383b),
('afk9k9al83s72drh0m84ndmkgnu4qdps', '::1', 1599652685, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635323638353b),
('rha7kgneojb8ptgmbmtubf30ubluucbi', '::1', 1599653323, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635333332333b),
('d3q2nnonnnbbqhbq1kjbkkmum3hqll0f', '::1', 1599653900, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635333930303b),
('kihrh340p296r3fh8a8ege0pgmbncbof', '::1', 1599654255, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635343235353b),
('rpkp1sqkotndr4bpqcct4njh4m2sg5v5', '::1', 1599654617, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635343631373b),
('lhj75q6ghg5rdouddakcdah14i81s9hr', '::1', 1599655084, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635353038343b),
('8h8fqr8rmbli0asadqo6mceak5bvc6cc', '::1', 1599655796, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635353739363b),
('hc21fqdi70q4vkhnehbtjmjnhilmjbpj', '::1', 1599656892, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635363839323b),
('eh8gtk3g57dtpechg1cu6du6m3i0sd13', '::1', 1599657626, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635373632363b),
('vqabr53qj2c1af1dnf5im86ntrq0b1gk', '::1', 1599658117, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635383131373b),
('po4c297b2sv2kmr0ndkt977dr7fods1f', '::1', 1599658475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635383437353b),
('oue5i43btk8jpqret1ttofbkgg8i00pv', '::1', 1599658794, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635383739343b),
('n7nhg7io6e7a3pae52pna948k7rt8ntr', '::1', 1599659476, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635393437363b),
('91h7bs596ltdqjfsqt85gg8t2k5tq7kg', '::1', 1599659784, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393635393738343b),
('m6q21dr6s0an3nsoo6lsu25fv5phmvoj', '::1', 1599660129, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636303132393b),
('lfkq7b93jurjqjmp1041esupgsdk23at', '::1', 1599660459, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636303435393b),
('l5gfnlgo1h0a92q0q4r5alnmjh6v29i2', '::1', 1599660899, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636303839393b),
('09uguoecgtiq94sfef29u0i4q33crkog', '::1', 1599661515, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636313531353b),
('c4km6242qiovb1nn7ql6c71llvtodl1o', '::1', 1599661851, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636313835313b),
('s614m0rfagssb918ren0v0fsont8obfp', '::1', 1599662753, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636323735333b),
('k6a1mupltq6vlqdvitlcm092k6rh2c8s', '::1', 1599663218, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636333231383b),
('div1a10cg5ti5sjcejcqbe6hkptsqtd4', '::1', 1599663961, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636333936313b),
('s5dj5dac50h8ojcbiflssm5v872vnrij', '::1', 1599664276, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636343237363b),
('6d7co2ts3pe5tetqkmr16hvqlrm48njt', '::1', 1599664516, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539393636343237363b);

-- --------------------------------------------------------

--
-- Estrutura da tabela `custo`
--

CREATE TABLE `custo` (
  `id_custo` int(11) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `valor` double NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `custo`
--

INSERT INTO `custo` (`id_custo`, `descricao`, `valor`, `data`) VALUES
(6, 'teste', 200, '2020-09-05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `nivel_acesso`
--

CREATE TABLE `nivel_acesso` (
  `id_nivel_acesso` int(11) NOT NULL,
  `nome_nivel_acesso` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `nivel_acesso`
--

INSERT INTO `nivel_acesso` (`id_nivel_acesso`, `nome_nivel_acesso`) VALUES
(1, 'Administrador');

-- --------------------------------------------------------

--
-- Estrutura da tabela `recebimento`
--

CREATE TABLE `recebimento` (
  `id_recebimento` int(11) NOT NULL,
  `id_venda` int(11) NOT NULL,
  `valor` double NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `servico`
--

CREATE TABLE `servico` (
  `id_servico` int(11) NOT NULL,
  `nome_servico` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `servico`
--

INSERT INTO `servico` (`id_servico`, `nome_servico`) VALUES
(1, 'Estampa de camisa'),
(2, 'Impressão');

-- --------------------------------------------------------

--
-- Estrutura da tabela `status_venda`
--

CREATE TABLE `status_venda` (
  `id_status_venda` int(11) NOT NULL,
  `nome_status_venda` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `status_venda`
--

INSERT INTO `status_venda` (`id_status_venda`, `nome_status_venda`) VALUES
(1, 'Pago'),
(2, 'Pagamento Pendente'),
(3, 'Agendamento');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_nivel_acesso` int(11) NOT NULL,
  `email` varchar(80) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `fl_ativo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `id_nivel_acesso`, `email`, `senha`, `fl_ativo`) VALUES
(1, 1, 'administrador@gmail.com', '$2y$10$xKNZn8BULnlb4hQk0YFoFuHNUYEJ.xdoWgDulYGFIJjm7H42XgoN.', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda`
--

CREATE TABLE `venda` (
  `id_venda` int(11) NOT NULL,
  `nome_cliente` varchar(80) NOT NULL,
  `valor` double NOT NULL,
  `id_status_venda` int(11) NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `data_realizacao_servico` date DEFAULT NULL,
  `data_agendamento` date DEFAULT NULL,
  `data_pagamento` datetime DEFAULT NULL,
  `telefone` varchar(16) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `valor_recebido` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda_servico`
--

CREATE TABLE `venda_servico` (
  `id_venda_servico` int(11) NOT NULL,
  `id_servico` int(11) NOT NULL,
  `id_venda` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `custo`
--
ALTER TABLE `custo`
  ADD PRIMARY KEY (`id_custo`);

--
-- Indexes for table `nivel_acesso`
--
ALTER TABLE `nivel_acesso`
  ADD UNIQUE KEY `id_nivel_acesso` (`id_nivel_acesso`);

--
-- Indexes for table `recebimento`
--
ALTER TABLE `recebimento`
  ADD PRIMARY KEY (`id_recebimento`),
  ADD KEY `fk_venda` (`id_venda`);

--
-- Indexes for table `servico`
--
ALTER TABLE `servico`
  ADD PRIMARY KEY (`id_servico`);

--
-- Indexes for table `status_venda`
--
ALTER TABLE `status_venda`
  ADD PRIMARY KEY (`id_status_venda`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `unic_email` (`email`),
  ADD KEY `fk_nivel_acesso` (`id_nivel_acesso`);

--
-- Indexes for table `venda`
--
ALTER TABLE `venda`
  ADD PRIMARY KEY (`id_venda`),
  ADD KEY `id_status_pagamento` (`id_status_venda`);

--
-- Indexes for table `venda_servico`
--
ALTER TABLE `venda_servico`
  ADD PRIMARY KEY (`id_venda_servico`),
  ADD KEY `fk_servico` (`id_servico`),
  ADD KEY `fk_venda` (`id_venda`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `custo`
--
ALTER TABLE `custo`
  MODIFY `id_custo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `nivel_acesso`
--
ALTER TABLE `nivel_acesso`
  MODIFY `id_nivel_acesso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `recebimento`
--
ALTER TABLE `recebimento`
  MODIFY `id_recebimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `servico`
--
ALTER TABLE `servico`
  MODIFY `id_servico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `status_venda`
--
ALTER TABLE `status_venda`
  MODIFY `id_status_venda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `venda`
--
ALTER TABLE `venda`
  MODIFY `id_venda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `venda_servico`
--
ALTER TABLE `venda_servico`
  MODIFY `id_venda_servico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `recebimento`
--
ALTER TABLE `recebimento`
  ADD CONSTRAINT `fk_vendas` FOREIGN KEY (`id_venda`) REFERENCES `venda` (`id_venda`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_nivel_acesso` FOREIGN KEY (`id_nivel_acesso`) REFERENCES `nivel_acesso` (`id_nivel_acesso`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `venda`
--
ALTER TABLE `venda`
  ADD CONSTRAINT `fk_status_venda` FOREIGN KEY (`id_status_venda`) REFERENCES `status_venda` (`id_status_venda`);

--
-- Limitadores para a tabela `venda_servico`
--
ALTER TABLE `venda_servico`
  ADD CONSTRAINT `fk_servico_venda` FOREIGN KEY (`id_servico`) REFERENCES `servico` (`id_servico`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venda_servico` FOREIGN KEY (`id_venda`) REFERENCES `venda` (`id_venda`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
