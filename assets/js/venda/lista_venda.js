function GerenciadorDeVenda() {
    this.vendas = null;

    this.setVendas = function (vendas) {
        this.vendas = vendas;
    }

    this.getVendas = function () {
        return this.vendas;
    }

    this.removerVenda = function (index) {
        this.vendas.splice(index, 1);
    }

    this.buscarVendas = function () {
        mostrarCarregando();
        let data = $("#data-vendas").val();
        $.ajax({
            method: 'GET',
            url: baseUrl(`Vendas/buscarPorData/${data}`),
            dataType: 'JSON'
        }).done(function (resposta) {
            gerVendas.setVendas(resposta.dados);
            gerVendas.atualizarListaVendas();
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    }

    this.atualizarListaVendas = function () {
        let vendas = gerVendas.getVendas();
        let totalVendido = 0;
        let totalRecebido = 0;
        let totalPendente = 0;
        let html = "";
        for (let i = 0; i < vendas.length; i++) {
            let servicos = vendas[i].servicos;
            let classeStatus = vendas[i].nome_status_venda === 'Pago' ? 'text-success' : 'text-danger';
            let descricao = vendas[i].descricao == null ? 'Sem descrição' : vendas[i].descricao;
            totalVendido += parseFloat(vendas[i].valor);
            totalRecebido += parseFloat(vendas[i].valor_recebido);

            if(vendas[i].id_status_venda == STUTUS_PAGAMENTO_PENDENTE){
                totalPendente += parseFloat(vendas[i].valor)-parseFloat(vendas[i].valor_recebido);
            }

            html += `
            <tr>
                <td>
                    <div class="servicos">
            `;

            for (let j = 0; j < servicos.length; j++) {
                html += `<span class="badge servico">${servicos[j].nome_servico}</span>&nbsp`;
            }

            html += `
                    </div>
                    <div class="dados"><strong>Cliente: </strong>${vendas[i].nome_cliente}</div>
                    <div class="dados"><strong>Valor: </strong>${converterMoedaUsuario(vendas[i].valor)}</div>
                    <div class="dados"><strong>Status: <span class="${classeStatus}">${vendas[i].nome_status_venda}</span></strong></div>
                    <div class="dados"><strong>Descrição: </strong>${descricao}</div>
                    <div style="text-align: right;">
                        <a href="${baseUrl('Vendas/paginaEditar/' + vendas[i].id_venda)}" class="btn btn-primary btn-sm">Editar</a>
                        <buttom class="btn btn-danger btn-sm btn-excluir" elemento-venda="${i}">Excluir</buttom>
                    </div>
                </td>
            </tr>               
            `;
        }

        $(".lista-vendas").html(html);
        $("#total").html(`
            <div class="col-md-4"><strong>Vendido: </strong><span>${converterMoedaUsuario(totalVendido)}</span></div>
            <div class="col-md-4"><strong>Recebido: </strong><span>${converterMoedaUsuario(totalRecebido)}</span></div>
            <div class="col-md-4"><strong>Pendente: </strong><span>${converterMoedaUsuario(totalPendente)}</span></div>
        `);
        this.addEventoBotoesListaVendas();
    }

    this.addEventoBotoesListaVendas = function () {
        $(".btn-excluir").click(function () {
            var index = $(this).attr('elemento-venda');
            let nomeCliente = gerVendas.getVendas()[index].nome_cliente;
            Swal.fire({
                title: 'Atenção',
                text: "Deseja realmente excluir a venda feita ao cliente " + nomeCliente + "?",
                icon: 'warning',
                confirmButtonColor: '#d33',
                showCancelButton: true,
                confirmButtonText: 'Excluir',
                cancelButtonText: 'Cancelar',
            }).then((result) => {
                if (result.value) {
                    mostrarCarregando();
                    let idVenda = gerVendas.getVendas()[index].id_venda;
                    $.ajax({
                        method: 'GET',
                        url: baseUrl(`Vendas/excluir/${idVenda}`),
                        dataType: 'JSON'
                    }).done(function (resposta) {
                        if (resposta.status == STATUS_SUCESSO) {
                            mostrarMsgSucesso(resposta.mensagem);
                            gerVendas.removerVenda(index);
                            gerVendas.atualizarListaVendas();
                        } else {
                            mostrarMsgErro(resposta.mensagem, resposta.status);
                        }
                    }).fail(function () {
                        mostrarMsgErro(MSG_ERRO_AJAX);
                    }).always(function () {
                        mostrarCarregando(false);
                    });
                }
            })
        });
    }
}

$(document).ready(function () {
    gerVendas = new GerenciadorDeVenda();
    gerVendas.buscarVendas();

    $("#buscar-venda-por-data").submit(function (e) {
        e.preventDefault();
        gerVendas.buscarVendas();
    });
});