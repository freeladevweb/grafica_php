const idForm = $("form").attr('id');

const adicionarEventoBtnExcluir = function () {
    $(".btn-excluir-servico-venda").click(function (e) {
        e.preventDefault();
        let statusVendaSalvo = $("#form-editar-venda input[name='status-venda-salvo']").val();

        if (statusVendaSalvo == STATUS_PAGO) {
            return;
        }

        let linha = $(this).parent().parent();
        $(linha).fadeOut("slow", function () {
            $(linha).remove();
        });
    });
}

const getServicos = function () {
    let servicos = [];
    let idVenda = $("#" + idForm + " input[name='id-venda']").val();
    $("#tbl-servicos-venda tr").each(function () {
        let servico = {
            id_servico: $(this).attr('id-servico'),
            id_venda: idVenda != undefined ? idVenda : ''
        };
        servicos.push(servico);
    });
    return servicos;
}

$(document).ready(function () {
    adicionarEventoBtnExcluir();

    if ($("#form-editar-venda input[name='status-venda-salvo']").val() == STATUS_PAGO) {
        $("#form-editar-venda input, #form-editar-venda select").attr('disabled', '')
    }

    $("#add-servico").click(function (e) {
        e.preventDefault();

        let idServico = $("#" + idForm + " select[name='servicos']").val();
        let nomeServico = $("#" + idForm + " select[name='servicos'] :selected").text();
        let servicos = getServicos();

        for (let i = 0; i < servicos.length; i++) {
            if (idServico == servicos[i].id_servico) {
                mostrarMsgErro("Serviço já adicionado.");
                return;
            }
        }

        if (idServico != null) {
            let html = `
            <tr id-servico="${idServico}">
                <td>${nomeServico}</td>
                <td style="text-align: right;">
                    <a href="#" class="btn btn-danger btn-sm btn-excluir-servico-venda">Excluir</a>
                </td>
            </tr>
            `;

            $("#tbl-servicos-venda tbody").append(html);
        }

    });

    $("#form-editar-venda").submit(function (e) {
        e.preventDefault();

        let statusVendaSalvo = $("#form-editar-venda input[name='status-venda-salvo']").val();
        let statusVendaNovo = $("#form-editar-venda select[name='status-venda']").val();

        if (statusVendaSalvo == STATUS_PAGO) {
            return;
        }

        let valorRecebido = $("#form-editar-venda input[name='valor-recebido-estatico']").val();
        let novoValorRecebido = $("#form-editar-venda input[name='valor-recebido']").val();
        novoValorRecebido = novoValorRecebido != '' ? converterValorSistema(novoValorRecebido) : 0;
        let totalRecebido = novoValorRecebido + parseFloat(valorRecebido);

        mostrarCarregando();
        let venda = {
            id_venda: $("#form-editar-venda input[name='id-venda']").val(),
            nome_cliente: $("#form-editar-venda input[name='nome-cliente']").val(),
            valor: $("#form-editar-venda input[name='valor']").val(),
            id_status_venda: statusVendaNovo,
            descricao: $("#form-editar-venda input[name='descricao']").val(),
            servicos: getServicos(),
            valor_recebido: totalRecebido,
            novo_valor_recebido: novoValorRecebido
        }

        $.ajax({
            method: 'POST',
            url: baseUrl(`Vendas/editar`),
            data: venda,
            dataType: 'JSON'
        }).done(function (resposta) {
            console.log(resposta.dados);
            if (resposta.status == STATUS_SUCESSO) {
                Swal.fire({
                    title: 'Atenção',
                    text: resposta.mensagem,
                    icon: STATUS_SUCESSO,
                    allowOutsideClick: false
                }).then(() => {
                    window.location.href = baseUrl(linkVoltar);
                });
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    });

    $("#form-editar-venda #select-status").change(function () {
        let status = $(this).val();

        if (status == STATUS_PAGO) {
            let valor = $("#form-editar-venda input[name='valor']").val();
            let valorRecebido = parseFloat($("#form-editar-venda input[name='valor-recebido-estatico']").val());
            valor = valor != '' ? converterValorSistema(valor) : 0;

            let novoValorRecebido = valor - valorRecebido;

            $("#form-editar-venda input[name='valor-recebido']").val(converterMoedaUsuario(novoValorRecebido, false));
        } else {
            $("#form-editar-venda input[name='valor-recebido']").val("");
        }
    });

    $("#form-cadastrar-venda #select-status").change(function () {
        let status = $(this).val();

        if (status == STATUS_PAGO) {
            $(".modo-pagamento-pendente").fadeOut();
        } else {
            $(".modo-pagamento-pendente").fadeIn();
        }
    });

    $("#form-editar-venda .modo-pagamento-pendente input, #form-editar-venda input[name='valor']").keyup(function () {
        let valor = $("#form-editar-venda input[name='valor']").val();
        let valorRecebido = $("#form-editar-venda input[name='valor-recebido-estatico']").val();
        let novoValorRecebido = $("#form-editar-venda input[name='valor-recebido']").val();
        let restante = "0,00"

        valor = valor != '' ? converterValorSistema(valor) : 0;
        novoValorRecebido = novoValorRecebido != '' ? converterValorSistema(novoValorRecebido) : 0;
        let totalRecebido = novoValorRecebido + parseFloat(valorRecebido);

        if (totalRecebido < valor) {
            restante = converterMoedaUsuario(valor - totalRecebido);
        }

        $("#form-editar-venda input[name='valor-restante']").val(restante);
    });

    $("#form-cadastrar-venda .modo-pagamento-pendente input, #form-cadastrar-venda input[name='valor']").keyup(function () {
        let valor = $("#form-cadastrar-venda input[name='valor']").val();
        let valorRecebido = $(".modo-pagamento-pendente input[name='valor-recebido']").val();
        let restante = "0,00"

        valor = valor != '' ? converterValorSistema(valor) : 0;
        valorRecebido = valorRecebido != '' ? converterValorSistema(valorRecebido) : 0;

        if (valor > valorRecebido) {
            restante = converterMoedaUsuario((valor - valorRecebido), false)
        }
        $("#form-cadastrar-venda input[name='valor-restante']").val(restante);
    });

    $("#form-cadastrar-venda").submit(function (e) {
        e.preventDefault();
        mostrarCarregando();
        let recebido = $("#form-cadastrar-venda input[name='valor-recebido']").val();
        let valor = $("#form-cadastrar-venda input[name='valor']").val();
        let status = $("#form-cadastrar-venda select[name='status-venda']").val();
        if (status == STATUS_PAGO) {
            recebido = valor;
        } else {
            if (recebido == '') {
                recebido = '0,00';
            }
        }

        let venda = {
            nome_cliente: $("#form-cadastrar-venda input[name='nome-cliente']").val(),
            valor: valor,
            id_status_venda: status,
            descricao: $("#form-cadastrar-venda input[name='descricao']").val(),
            valor_recebido: recebido,
            servicos: getServicos()
        }

        $.ajax({
            method: 'POST',
            url: baseUrl(`Vendas/cadastrar`),
            data: venda,
            dataType: 'JSON'
        }).done(function (resposta) {
            console.log(resposta.dados);
            if (resposta.status == STATUS_SUCESSO) {
                Swal.fire({
                    title: 'Atenção',
                    text: resposta.mensagem,
                    icon: STATUS_SUCESSO,
                    allowOutsideClick: false
                }).then(() => {
                    window.location.href = baseUrl('Vendas');
                });
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    })


})