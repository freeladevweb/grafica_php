const BASE_URL = 'http://localhost/grafica_php/';

const MSG_ERRO_AJAX = 'Não foi possível concluir a ação, recarregue a página e tente novamente';

const STATUS_SUCESSO = "success";
const STATUS_ERROR = 'error';
const STATUS_ATENCAO = 'warning';

const STATUS_PAGO = 1;
const STUTUS_PAGAMENTO_PENDENTE = 2;

const mostrarCarregando = (carregando = true) => {
    let elem = '#carregando';
    if (carregando) {
        $(elem).show();
    } else {
        $(elem).hide();
    }
};

const baseUrl = (url = '') => {
    return `${BASE_URL}${url}`;
};

const mostrarMsgSucesso = (msg) => {
    Swal.fire({ icon: STATUS_SUCESSO, title: 'Sucesso', text: msg });
}

const mostrarMsgErro = (msg, icon = STATUS_ERROR) => {
    Swal.fire({ icon: icon, title: 'Erro', text: msg });
}

const converterMoedaUsuario = function(valor, addSimbolo = true){
    valor = parseFloat(valor);
    if(addSimbolo){
        return valor.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    }
    return valor.toLocaleString('pt-br', {minimumFractionDigits: 2});
}

const converterValorSistema = function(stringValor) {
    if(stringValor == ''){
        return 0;
    }
    let valor = stringValor.replace(/[.]/g, '');
    valor = valor.replace(/[,]/g, ".");
    return parseFloat(valor);
}
