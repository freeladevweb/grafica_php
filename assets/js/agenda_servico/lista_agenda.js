$(document).ready(function () {
    $(".btn-excluir").click(function () {
        var index = $(this);
        Swal.fire({
            title: 'Atenção',
            text: "Deseja realmente excluir esse agendamento?",
            icon: 'warning',
            confirmButtonColor: '#d33',
            showCancelButton: true,
            confirmButtonText: 'Excluir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.value) {
                mostrarCarregando();
                let idAgendamento = $(index).attr('id-agendamento');
                $.ajax({
                    method: 'GET',
                    url: baseUrl(`AgendaServicos/excluir/${idAgendamento}`),
                    dataType: 'JSON'
                }).done(function (resposta) {
                    if (resposta.status == STATUS_SUCESSO) {
                        mostrarMsgSucesso(resposta.mensagem);
                        $(index).parent().parent().fadeOut();
                    } else {
                        mostrarMsgErro(resposta.mensagem, resposta.status);
                    }
                }).fail(function () {
                    mostrarMsgErro(MSG_ERRO_AJAX);
                }).always(function () {
                    mostrarCarregando(false);
                });
            }
        })
    });
});