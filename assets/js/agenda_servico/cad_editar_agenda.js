const idForm = $("form").attr('id');

const getServicos = function () {
    let servicos = [];
    let idVenda = $("#" + idForm + " input[name='id-venda']").val();
    $("#tbl-servicos-venda tr").each(function () {
        let servico = {
            id_servico: $(this).attr('id-servico'),
            id_venda: idVenda != undefined ? idVenda : ''
        };
        servicos.push(servico);
    });
    return servicos;
}

$(document).ready(function () {
    $("#add-servico").click(function (e) {
        e.preventDefault();

        let idServico = $("#" + idForm + " select[name='servicos']").val();
        let nomeServico = $("#" + idForm + " select[name='servicos'] :selected").text();
        let servicos = getServicos();

        for (let i = 0; i < servicos.length; i++) {
            if (idServico == servicos[i].id_servico) {
                mostrarMsgErro("Serviço já adicionado.");
                return;
            }
        }

        if (idServico != null) {
            let html = `
            <tr id-servico="${idServico}">
                <td>${nomeServico}</td>
                <td style="text-align: right;">
                    <a href="#" class="btn btn-danger btn-sm btn-excluir-servico-venda">Excluir</a>
                </td>
            </tr>
            `;

            $("#tbl-servicos-venda tbody").append(html);
        }
    });

    $("#form-editar-agendamento").submit(function (e) {
        e.preventDefault();
        mostrarCarregando();
        let venda = {
            id_venda: $("#form-editar-agendamento input[name='id-venda']").val(),
            nome_cliente: $("#form-editar-agendamento input[name='nome-cliente']").val(),
            valor: $("#form-editar-agendamento input[name='valor']").val(),
            descricao: $("#form-editar-agendamento input[name='descricao']").val(),
            data_agendamento: $("#form-editar-agendamento input[name='data-agendamento']").val(),
            telefone: $("#form-editar-agendamento input[name='telefone']").val(),
            servicos: getServicos()
        }

        $.ajax({
            method: 'POST',
            url: baseUrl(`AgendaServicos/editar`),
            data: venda,
            dataType: 'JSON'
        }).done(function (resposta) {
            if (resposta.status == STATUS_SUCESSO) {
                Swal.fire({
                    title: 'Atenção',
                    text: resposta.mensagem,
                    icon: STATUS_SUCESSO,
                    allowOutsideClick: false
                }).then(() => {
                    window.location.href = baseUrl('AgendaServicos');
                });
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    });

    $("#form-cadastrar-agendamento").submit(function (e) {
        e.preventDefault();
        mostrarCarregando();
        let venda = {
            nome_cliente: $("#form-cadastrar-agendamento input[name='nome-cliente']").val(),
            valor: $("#form-cadastrar-agendamento input[name='valor']").val(),
            descricao: $("#form-cadastrar-agendamento input[name='descricao']").val(),
            data_agendamento: $("#form-cadastrar-agendamento input[name='data-agendamento']").val(),
            telefone: $("#form-cadastrar-agendamento input[name='telefone']").val(),
            servicos: getServicos()
        }

        $.ajax({
            method: 'POST',
            url: baseUrl(`AgendaServicos/cadastrar`),
            data: venda,
            dataType: 'JSON'
        }).done(function (resposta) {
            if (resposta.status == STATUS_SUCESSO) {
                Swal.fire({
                    title: 'Atenção',
                    text: resposta.mensagem,
                    icon: STATUS_SUCESSO,
                    allowOutsideClick: false
                }).then(() => {
                    window.location.href = baseUrl('AgendaServicos');
                });
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    });

    $("#btn-gerar-venda").click(function (e) {
        e.preventDefault();
        Swal.fire({
            title: 'Atenção',
            text: "Deseja realmente gerar um venda a partir deste agendamento? Esse agendamento será excluído automaticamente.",
            icon: 'warning',
            confirmButtonColor: '#d33',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.value) {
                mostrarCarregando();
                let dados = {
                    id_venda: $("#form-editar-agendamento input[name='id-venda']").val()
                };
                console.log(dados)
                $.ajax({
                    method: 'POST',
                    url: baseUrl(`AgendaServicos/gerarVenda`),
                    data: dados,
                    dataType: 'JSON'
                }).done(function (resposta) {
                    if (resposta.status == STATUS_SUCESSO) {
                        Swal.fire({
                            title: 'Atenção',
                            text: resposta.mensagem,
                            icon: STATUS_SUCESSO,
                            allowOutsideClick: false
                        }).then(() => {
                            let id = $("#form-editar-agendamento input[name='id-venda']").val();
                            window.location.href = baseUrl('Vendas/paginaEditar/'+id);
                        });
                    } else {
                        mostrarMsgErro(resposta.mensagem, resposta.status);
                    }
                }).fail(function () {
                    mostrarMsgErro(MSG_ERRO_AJAX);
                }).always(function () {
                    mostrarCarregando(false);
                });
            }
        })

    });
});