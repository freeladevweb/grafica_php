function getUsuario() {
    let usuario = {
        email: $("#form-login input[name='email']").val(),
        senha: $("#form-login input[name='senha']").val()
    };
    return usuario;
}

$(document).ready(function () {
    $('#form-login').submit(function (e) {
        e.preventDefault();
        mostrarCarregando();
        $.ajax({
            method: $(this).attr('method'),
            url: $(this).attr('action'),
            data: getUsuario(),
            dataType: 'JSON'
        }).done(function (resposta) {
            if(resposta.status === STATUS_ATENCAO || resposta.status === STATUS_ERROR){
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }else{
                window.location.href = baseUrl('Home');
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });;
    })
});