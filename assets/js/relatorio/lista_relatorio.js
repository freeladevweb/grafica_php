$(document).ready(function () {
    buscarRelatorio();

    $("#buscar-relatorio").submit(function(e){
        e.preventDefault();
        buscarRelatorio();
    });
});

function buscarRelatorio() {
    de = $("#buscar-relatorio input[name='de']").val();
    ate = $("#buscar-relatorio input[name='ate']").val();
    $("#vendido").html("Aguarde");
    $("#recebido").html("Aguarde");
    $("#pendente").html("Aguarde");
    $("#custos").html("Aguarde");
    $("#lucro").html("Aguarde");
    $.ajax({
        method: 'GET',
        url: baseUrl(`Relatorio/buscar/${de}/${ate}`),
        dataType: 'JSON'
    }).done(function (resposta) {
        if (resposta.status == STATUS_SUCESSO) {
            $("#vendido").html(converterMoedaUsuario(resposta.dados.vendido));
            $("#recebido").html(converterMoedaUsuario(resposta.dados.recebido));
            $("#pendente").html(converterMoedaUsuario(resposta.dados.pendente));
            $("#custos").html(converterMoedaUsuario(resposta.dados.custos));
            $("#lucro").html(converterMoedaUsuario(resposta.dados.lucro));
        } else {
            $("#vendido").html("R$ 0,00");
            $("#recebido").html("R$ 0,00");
            $("#pendente").html("R$ 0,00");
            $("#custos").html("R$ 0,00");
            $("#lucro").html("R$ 0,00");
            mostrarMsgErro(resposta.mensagem, resposta.status);
        }
    }).fail(function () {
        mostrarMsgErro(MSG_ERRO_AJAX);
    });
}