const MODO_EDITAR = 1;
const MODO_CADASTRAR = 2;
var gerServicos = null;

function GerenciadorDeServico() {
    this.listaDeSevicos = [];
    this.servicoSelecionado = null;
    this.indexServicoSelecionado = null;

    this.getServico = function (index) {
        return listaDeSevicos[index];
    }

    this.adicionarServicos = function (servicos) {
        listaDeSevicos = servicos;
        this.atualizarTabela();
    }

    this.acrescentarServico = function (servico) {
        listaDeSevicos.push(servico);
        this.atualizarTabela();
    }

    this.removerServico = function (index) {
        listaDeSevicos.splice(index, 1);
        this.atualizarTabela();

    }

    this.atualizarTabela = function () {
        $('#servicos tbody').html('');

        let html = '';

        for (let i = 0; i < listaDeSevicos.length; i++) {
            html += `
            <tr>
                <td>${listaDeSevicos[i].nome_servico}</td>
                <td>
                    <button href="#" data-servico="${i}" class="btn btn-primary btn-sm btn-editar-servico">Editar</button>
                    <button href="#" data-servico="${i}" class="btn btn-danger btn-sm btn-excluir">Excluir</button>
                </td>
            </tr>
            `;
        }
        $('#servicos tbody').html(html);
        this.addEventoBotoesOpcoes();
    }

    this.addEventoBotoesOpcoes = function () {
        $('.btn-editar-servico').click(function () {
            let index = $(this).attr('data-servico');
            gerServicos.servicoSelecionado = listaDeSevicos[index];
            gerServicos.indexServicoSelecionado = index;
            $("#titulo-modal-servico").html('Editar Serviço');
            $("#modal-cadastrar-editar-servico input[name='modo-form-servico']").val(MODO_EDITAR);
            $("#modal-cadastrar-editar-servico input[name='nome_servico']").val(gerServicos.servicoSelecionado.nome_servico);
            $('#modal-cadastrar-editar-servico').modal('show');
        });

        this.excluirServico();
    }

    this.excluirServico = function () {
        $('.btn-excluir').click(function () {
            gerServicos.servicoSelecionado = gerServicos.getServico($(this).attr('data-servico'));

            Swal.fire({
                title: 'Atenção',
                text: "Deseja realmente excluir o serviço " + gerServicos.servicoSelecionado,
                icon: 'warning',
                confirmButtonColor: '#d33',
                showCancelButton: true,
                confirmButtonText: 'Excluir',
                cancelButtonText: 'Cancelar',

            }).then((result) => {
                if (result.value) {
                    mostrarCarregando();
                    $.ajax({
                        method: 'GET',
                        url: baseUrl('Servicos/excluir/' + gerServicos.servicoSelecionado.id_servico),
                        dataType: 'JSON'
                    }).done(function (resposta) {
                        if (resposta.status === STATUS_SUCESSO) {
                            let index = $(this).attr('data-servico');
                            mostrarMsgSucesso(resposta.mensagem);
                            gerServicos.removerServico(index);
                        } else {
                            mostrarMsgErro(resposta.mensagem, resposta.status);
                        }
                    }).fail(function () {
                        mostrarMsgErro(MSG_ERRO_AJAX);
                    }).always(function () {
                        mostrarCarregando(false);
                    });
                }
            })


        });
    }

    this.buscarServicos = function () {
        mostrarCarregando();
        $.ajax({
            method: 'GET',
            url: baseUrl('Servicos/buscarTodos'),
            dataType: 'JSON'
        }).done(function (resposta) {
            gerServicos.adicionarServicos(resposta.dados);
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    }

    this.editarServico = function () {
        let valorInformado = $("#form-cadastrar-editar-servico input[name='nome_servico']").val();
        if (gerServicos.servicoSelecionado.nome_servico === valorInformado) {
            mostrarMsgErro('Nenhuma alteção foi realizada.', STATUS_ATENCAO);
            return;
        }
        gerServicos.servicoSelecionado.nome_servico = valorInformado
        mostrarCarregando();
        $.ajax({
            method: 'POST',
            url: baseUrl('Servicos/editar'),
            data: gerServicos.servicoSelecionado,
            dataType: 'JSON'
        }).done(function (resposta) {
            if (resposta.status === STATUS_SUCESSO) {
                $('#modal-cadastrar-editar-servico').modal('hide');
                mostrarMsgSucesso(resposta.mensagem);
                gerServicos.listaDeSevicos[gerServicos.indexServicoSelecionado] = gerServicos.servicoSelecionado;
                gerServicos.atualizarTabela();
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    }

    this.cadastrarServico = function () {
        let servico = {
            nome_servico: $("#form-cadastrar-editar-servico input[name='nome_servico']").val()
        };
        mostrarCarregando();
        $.ajax({
            method: 'POST',
            url: baseUrl('Servicos/cadastrar'),
            data: servico,
            dataType: 'JSON'
        }).done(function (resposta) {
            if (resposta.status === STATUS_SUCESSO) {
                $('#modal-cadastrar-editar-servico').modal('hide');
                mostrarMsgSucesso(resposta.mensagem);
                gerServicos.acrescentarServico(resposta.dados);
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    }
}

$(document).ready(function () {
    gerServicos = new GerenciadorDeServico();
    gerServicos.buscarServicos();

    $("#form-cadastrar-editar-servico").submit(function (e) {
        e.preventDefault();
        let modo = $("#form-cadastrar-editar-servico input[name = 'modo-form-servico']").val();

        if (modo == MODO_EDITAR) {
            gerServicos.editarServico();
        } else if (modo == MODO_CADASTRAR) {
            gerServicos.cadastrarServico();
        }
    });

    $('#abrir-modal-cadastrar').click(function () {
        $("#titulo-modal-servico").html('Cadastrar Serviço');
        $("#modal-cadastrar-editar-servico input[name='modo-form-servico']").val(MODO_CADASTRAR);
        $("#form-cadastrar-editar-servico input[name='nome_servico']").val('');
        $('#modal-cadastrar-editar-servico').modal('show');
    });
});