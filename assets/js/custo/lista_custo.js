const buscarCustos = function () {
    mostrarCarregando();
    let data = $("#data-custo").val();
    $.ajax({
        method: 'GET',
        url: baseUrl('Custos/buscarPorData/' + data),
        dataType: 'JSON'
    }).done(function (resposta) {
        if (resposta.status == STATUS_SUCESSO) {
            montarTabela(resposta.dados);
        } else {
            window.location.href = baseUrl();
        }
    }).fail(function () {
        mostrarMsgErro(MSG_ERRO_AJAX);
    }).always(function () {
        mostrarCarregando(false);
    });
}

const montarTabela = function (listaDeCustos) {
    $('#custos tbody').html('');

    let total = 0;
    let html = '';

    for (let i = 0; i < listaDeCustos.length; i++) {
        total += parseFloat(listaDeCustos[i].valor);
        html += `
        <tr>
            <td>
                <div><strong>Descrição: </strong>${listaDeCustos[i].descricao}</div>
                <div><strong>Valor: </strong>${converterMoedaUsuario(listaDeCustos[i].valor)}</div>
            </td>
            <td>
                <a href="${baseUrl('Custos/paginaEditar/' + listaDeCustos[i].id_custo)}" class="btn btn-primary btn-sm btn-editar-custo">Editar</a>
                <button href="#" data-custo="${listaDeCustos[i].id_custo}" class="btn btn-danger btn-sm btn-excluir">Excluir</button>
            </td>
        </tr>
        `;
    }
    $('#custos tbody').html(html);
    montarTotal(converterMoedaUsuario(total));    
    addEventoBtnExcluir();
}

const montarTotal = function(total){
    $('#total').html(`<div class='col col-12'><strong>Total: </strong>${total}</div>`);
}

const addEventoBtnExcluir = function () {

    $(".btn-excluir").click(function () {

        Swal.fire({
            title: 'Atenção',
            text: "Deseja realmente excluir este custo?",
            icon: 'warning',
            confirmButtonColor: '#d33',
            showCancelButton: true,
            confirmButtonText: 'Excluir',
            cancelButtonText: 'Cancelar',
        }).then((result) => {
            if (result.value) {
                var elem = $(this).parent().parent();
                let id = $(this).attr('data-custo');
                mostrarCarregando();
                $.ajax({
                    method: 'GET',
                    url: baseUrl('Custos/excluir/' + id),
                    dataType: 'JSON'
                }).done(function (resposta) {
                    if (resposta.status == STATUS_SUCESSO) {
                        mostrarMsgSucesso(resposta.mensagem);
                        buscarCustos();                        
                    } else {
                        mostrarMsgErro(resposta.mensagem, resposta.status);
                    }
                }).fail(function () {
                    mostrarMsgErro(MSG_ERRO_AJAX);
                }).always(function () {
                    mostrarCarregando(false);
                });

            }
        })


    });
}

$(document).ready(function () {
    buscarCustos();

    $("#buscar-custo-por-data").submit(function(e){
        e.preventDefault();
        buscarCustos();
    });
});