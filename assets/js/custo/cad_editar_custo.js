$(document).ready(function () {
    $("#form-editar-custo").submit(function (e) {
        e.preventDefault();
        let custo = {
            id_custo: $("input[name='id-custo']").val(),
            descricao: $("input[name='descricao']").val(),
            valor: $("input[name='valor']").val(),
            data: $("input[name='data']").val()
        }

        mostrarCarregando();
        $.ajax({
            method: 'POST',
            url: baseUrl('Custos/editar'),
            data: custo,
            dataType: 'JSON'
        }).done(function (resposta) {
            if (resposta.status == STATUS_SUCESSO) {
                Swal.fire({
                    title: 'Atenção',
                    text: resposta.mensagem,
                    icon: STATUS_SUCESSO,
                    allowOutsideClick: false
                }).then(() => {
                    window.location.href = baseUrl('Custos');
                });
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    });

    $("#form-cadastrar-custo").submit(function (e) {
        e.preventDefault();
        let custo = {
            descricao: $("input[name='descricao']").val(),
            valor: $("input[name='valor']").val(),
            data: $("input[name='data']").val()
        }

        mostrarCarregando();
        $.ajax({
            method: 'POST',
            url: baseUrl('Custos/cadastrar'),
            data: custo,
            dataType: 'JSON'
        }).done(function (resposta) {
            if (resposta.status == STATUS_SUCESSO) {
                Swal.fire({
                    title: 'Atenção',
                    text: resposta.mensagem,
                    icon: STATUS_SUCESSO,
                    allowOutsideClick: false
                }).then(() => {
                    window.location.href = baseUrl('Custos');
                });
            } else {
                mostrarMsgErro(resposta.mensagem, resposta.status);
            }
        }).fail(function () {
            mostrarMsgErro(MSG_ERRO_AJAX);
        }).always(function () {
            mostrarCarregando(false);
        });
    });
});