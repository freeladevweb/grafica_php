<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustosModel extends CI_Model {

    public function buscarPorId($id){
        $this->db->select('*');
        $this->db->where('id_custo', $id);
        return $this->db->get('custo')->result();
    }

    public function buscarTodos($data){
        $this->db->select('*');
        $this->db->where('data', $data);
        return $this->db->get('custo')->result();
    }

    public function somarPorDataLimite($de, $ate){
        $this->db->select_sum('valor');
        $this->db->where('data >=', $de);
        $this->db->where('data <=', $ate);
        return $this->db->get('custo')->result();
    } 

    public function cadastrar($custo){
        return $this->db->insert('custo', $custo);
    }

    public function excluir($id){
        $this->db->where('id_custo', $id);
        return $this->db->delete('custo');
    }

    public function editar($id, $custo){
        $this->db->where('id_custo', $id);
        return $this->db->update('custo', $custo);
    }
}