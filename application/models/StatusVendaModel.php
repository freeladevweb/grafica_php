<?php
defined('BASEPATH') or exit('No direct script access allowed');

class StatusVendaModel extends CI_Model
{
    public function buscar()
    {
        $this->db->select('*');
        return $this->db->get('status_venda')->result();
    }
}