<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UsuarioModel extends CI_Model
{
    public function buscarPorEmail($email){
        $this->db->select('*');
        $this->db->where('email', $email);
        return $this->db->get('usuario')->result();
    }
}