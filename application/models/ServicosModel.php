<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServicosModel extends CI_Model {

    public function buscarTodos(){
        $this->db->select('*');
        return $this->db->get('servico')->result();
    }

    public function cadastrar($servico){
        $this->db->insert('servico', $servico);
        return $this->db->insert_id();
    }

    public function excluir($idServico){
        $this->db->where('id_servico', $idServico);
        return $this->db->delete('servico');
    }

    public function editar($idServico, $servico){
        $this->db->where('id_servico', $idServico);
        return $this->db->update('servico', $servico);
    }

    public function podeExcluir($id){
        $this->db->select('*');
        $this->db->where('id_servico', $id);
        $this->db->limit(1, 0);
        $result = $this->db->get('venda_servico')->result();

        if($result){
            return false;
        }

        return true;

	}
}