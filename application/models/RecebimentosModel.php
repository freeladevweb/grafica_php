<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RecebimentosModel extends CI_Model{
    public function cadastrar($recebimento){
        return $this->db->insert('recebimento', $recebimento);
    }

    public function somarPorDataLimite($de, $ate){
        $this->db->select_sum('valor');
        $this->db->where('data >=', $de);
        $this->db->where('data <=', $ate);
        return $this->db->get('recebimento')->result();
    } 
}