<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VendasModel extends CI_Model
{
    public function cadastrar($venda){
        $this->db->insert('venda', $venda);
        return $this->db->insert_id();
    }

    public function buscar($data)
    {
        $this->db->select('*');
        $this->db->from('venda');
        $this->db->select('status_venda.*');
        $this->db->join('status_venda', 'venda.id_status_venda = status_venda.id_status_venda');
        $this->db->where('venda.data_realizacao_servico', $data);
        $this->db->where('venda.id_status_venda !=', STATUS_VENDA_AGENDAMENTO);
        return $this->db->get()->result();
    }

    public function buscarPorId($id){
        $this->db->select('*');
        $this->db->select('status_venda.*');
        $this->db->select('venda.*');
        $this->db->select('servico.*');
        $this->db->from('venda_servico');
        $this->db->join('venda', 'venda.id_venda = venda_servico.id_venda');
        $this->db->join('servico', 'servico.id_servico = venda_servico.id_servico');
        $this->db->join('status_venda', 'status_venda.id_status_venda = venda.id_status_venda');
        $this->db->where('venda.id_status_venda !=', STATUS_VENDA_AGENDAMENTO);
        $this->db->where('venda.id_venda', $id);
        return $this->db->get()->result();
    }

    public function buscarAgendadas($data){
        $this->db->select('*');
        $this->db->from('venda');
        $this->db->select('status_venda.*');
        $this->db->join('status_venda', 'venda.id_status_venda = status_venda.id_status_venda');
        $this->db->where('venda.data_agendamento', $data);
        $this->db->where('venda.id_status_venda', STATUS_VENDA_AGENDAMENTO);
        return $this->db->get()->result();
    }

    public function buscarAgendadoPorId($id){
        $this->db->select('*');
        $this->db->select('status_venda.*');
        $this->db->select('venda.*');
        $this->db->select('servico.*');
        $this->db->from('venda_servico');
        $this->db->join('venda', 'venda.id_venda = venda_servico.id_venda');
        $this->db->join('servico', 'servico.id_servico = venda_servico.id_servico');
        $this->db->join('status_venda', 'status_venda.id_status_venda = venda.id_status_venda');
        $this->db->where('venda.id_status_venda', STATUS_VENDA_AGENDAMENTO);
        $this->db->where('venda.id_venda', $id);
        return $this->db->get()->result();
    }

    public function buscarTodasPendentes($pagina){
        $this->db->select('*');
        $this->db->where('id_status_venda', STATUS_VENDA_PAGAMENTO_PENDENTE);
        $this->db->limit(LIMITE_PAGINACAO, $pagina);
        return $this->db->get('venda')->result();
    }

    public function buscarPendentesPorCliente($cliente){
        $this->db->select('*');
        $this->db->where('id_status_venda', STATUS_VENDA_PAGAMENTO_PENDENTE);
        $this->db->like('nome_cliente', $cliente); 
        return $this->db->get('venda')->result();
    }

    public function somarPorDataLimite($de, $ate){
        $this->db->select_sum('valor');
        $this->db->where('data_realizacao_servico >=', $de);
        $this->db->where('data_realizacao_servico <=', $ate);
        $this->db->where('venda.id_status_venda !=', STATUS_VENDA_AGENDAMENTO);
        return $this->db->get('venda')->result();
    } 

    public function editar($id, $venda){
        $this->db->where('id_venda', $id);
        return $this->db->update('venda', $venda);
    }

    public function excluir($id){
        $this->db->where('id_venda', $id);
        return $this->db->delete('venda');
    }

    public function contarTodos($data)
    {
        $this->db->where('venda.data_realizacao_servico', $data);
        return $this->db->from("venda")->count_all_results();
    }

    public function contarTodasPendente(){
        $this->db->where('venda.id_status_venda', STATUS_VENDA_PAGAMENTO_PENDENTE);
        return $this->db->from("venda")->count_all_results();
    }
}
