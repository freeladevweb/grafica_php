<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VendasServicosModel extends CI_Model
{
    public function buscar($idVendaDe, $idVendaAte)
    {
        $this->db->select('*');
        $this->db->from('venda_servico');
        $this->db->select('servico.*');
        $this->db->where('id_venda >= ', $idVendaDe);
        $this->db->where('id_venda <= ', $idVendaAte);
        $this->db->join('servico', 'servico.id_servico = venda_servico.id_servico');
        $this->db->order_by('venda_servico.id_venda');
        return $this->db->get()->result();
    }

    public function cadastrar($vendasServicos)
    {
        return $this->db->insert_batch('venda_servico', $vendasServicos);
    }

    public function excluirPorIdVenda($idVenda)
    {
        $this->db->where('id_venda', $idVenda);
        return $this->db->delete('venda_servico');
    }
}
