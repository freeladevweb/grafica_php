<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function index()
	{
		if (isset($sessao['logado'])) {
			redirect(base_url());
		}
		$this->load->view('LoginView');
	}

	public function entrar() {
        if ($this->session->has_userdata('id_usuario')) {
            retornarJson("Existe um usuário logado.",STATUS_ERRO);
            return;
        }
        $this->load->model('UsuarioModel', 'usuario');
        $email = trim(strtolower($this->input->post('email')));
        $senha = $this->input->post('senha');
        $usuario = $this->usuario->buscarPorEmail($email);

        if ($usuario) {
            if (!password_verify($senha, $usuario[0]->senha)) {
                retornarJson(STATUS_ERRO, "Usuário ou senha incorreta.");
                return;
            }

            if ($usuario[0]->fl_ativo == false) {
                retornarJson(STATUS_ERRO, "Usuário inativo.");
                return;
            }

            $dadosSessao = [
                'id_usuario' => $usuario[0]->id_usuario,
                'email' => $usuario[0]->email,
                'logado' => true,
                'id_nivel_acesso' => $usuario[0]->id_nivel_acesso
            ];

            $this->session->set_userdata($dadosSessao);

            retornarJson( 'Logado com sucesso.',STATUS_SUCESSO, $dadosSessao);
        } else {
            retornarJson("Usuário ou senha incorreta.", STATUS_ERRO);
        }
    }

    public function sair() {
        $this->session->sess_destroy();
        redirect(base_url('Login'));
    }
}
