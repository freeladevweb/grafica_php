<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vendas extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		verificarPermissao($this->session->userdata());
		$this->load->model('VendasModel', 'venda');
		$this->load->model('ServicosModel', 'servico');
		$this->load->model('StatusVendaModel', 'status');
		$this->load->model('RecebimentosModel', 'recebimento');
		$this->load->model('VendasServicosModel', 'vendasservicos');
	}

	public function index()
	{
		$this->load->model('ServicosModel', 'servico');
		$this->load->model('StatusVendaModel', 'status');
		$dadosView = [
			'data' => date('Y-m-d'),
			'servicos' => $this->servico->buscarTodos(),
			'statusVenda' => $this->status->buscar()
		];
		$this->load->view("vendas/ListaVendasView", $dadosView);
	}

	public function paginaEditar($id = '', $paginaRetorno='Vendas')
	{
		if (is_numeric($id)) {
			$vendas = $this->venda->buscarPorId($id);
			if ($vendas) {
				$dadosView = [
					'servicos' => $this->servico->buscarTodos(),
					'statusVenda' => $this->status->buscar(),
					'venda' => $vendas,
					'paginaRetorno' => $paginaRetorno
				];
				$this->load->view('vendas/EditarVendaView', $dadosView);
				return;
			}
		}
		redirect(base_url('Vendas'));
	}

	public function paginaCadastrar()
	{
		$dadosView = [
			'servicos' => $this->servico->buscarTodos(),
			'statusVenda' => $this->status->buscar()
		];
		$this->load->view('vendas/CadastrarVendaView', $dadosView);
	}

	/**
	 * Lista todos as vendas com seus respectivos produtos
	 *
	 * @param string $data
	 * @param integer $pagina
	 * @return void
	 */
	public function buscarPorData($data = '')
	{
		$dataValida = validarData($data, 'Y-m-d');

		if ($dataValida) {
			$vendas = $this->venda->buscar($data);

			if (!$vendas) {
				retornarJson("Nenhuma venda para a data " . converterDataParaUsuario($data) . " foi encontrada. ", STATUS_ERRO);
				return;
			}

			$servicosDeVenda = $this->vendasservicos->buscar($vendas[0]->id_venda, end($vendas)->id_venda);
			$vendasParaRetorno = [];
			$index = 0;

			foreach ($vendas as $venda) {
				$servicos = [];
				foreach ($servicosDeVenda as $servico) {
					if ($servico->id_venda != $venda->id_venda) {
						break;
					}

					$servicos[] = $servico;
					unset($servicosDeVenda[$index]);
					$index++;
				}

				$venda->servicos = $servicos;
				$vendasParaRetorno[] = $venda;
			}

			retornarJson('', STATUS_SUCESSO, $vendasParaRetorno);
		} else {
			retornarJson('Informe uma data válida.', STATUS_INVALIDO);
		}
	}

	public function cadastrar()
	{
		$venda = $this->input->post();

		$vendaValida = $this->vendaValida($venda);

		if ($vendaValida) {
			$this->load->model('VendasServicosModel', 'vendasservico');
			$servicosSemIdVenda = $venda['servicos'];
			$servicosCadastrar = [];
			unset($venda['servicos']);
			$venda['valor'] = converterValorDB($venda['valor']);
			$venda['data_cadastro'] = date('Y-m-d H:i:s');
			$venda['data_realizacao_servico'] = date('Y-m-d');

			if ($venda['id_status_venda'] == STATUS_VENDA_PAGO) {
				$venda['data_pagamento'] = date('Y-m-d');
			}

			$idVenda = $this->venda->cadastrar($venda);

			if (!is_numeric($idVenda)) {
				retornarJson("Algo deu errado. Tente salvar novamente.", STATUS_ERRO);
				return;
			}

			foreach ($servicosSemIdVenda as $servico) {
				$servico['id_venda'] = $idVenda;
				$servicosCadastrar[] = $servico;
			}

			$cadastrouServicos = $this->vendasservico->cadastrar($servicosCadastrar);
			
			if($venda['valor_recebido'] != '0,00' && $venda['valor_recebido'] != ''){
				$this->recebimento->cadastrar([
					'id_venda' => $idVenda,
					'valor' => converterValorDB($venda['valor_recebido']),
					'data' => date('Y-m-d')
				]);
			}


			if ($cadastrouServicos) {
				retornarJson("Venda cadastrada com sucesso.", STATUS_SUCESSO);
			} else {
				retornarJson("Algo deu errado. Tente cadastrar novamente.", STATUS_ERRO);
			}
		} else {
			retornarJson('Verifique se todos os campos foram preenchidos.', STATUS_INVALIDO);
		}
	}

	public function editar()
	{
		$venda = $this->input->post();

		$vendaValida = $this->vendaValida($venda);

		if ($vendaValida) {
			$this->load->model('VendasServicosModel', 'vendasservico');
			$servicos = $venda['servicos'];
			$idVenda = $venda['id_venda'];
			$novoValorRecebido = $venda['novo_valor_recebido'];
			unset($venda['servicos']);
			unset($venda['id_venda']);
			unset($venda['novo_valor_recebido']);
			$venda['valor'] = converterValorDB($venda['valor']);
			$venda['id_status_venda'] = $venda['valor'] <=  $venda['valor_recebido'] ? STATUS_VENDA_PAGO: STATUS_VENDA_PAGAMENTO_PENDENTE;

			$excluiuVendasServicos = $this->vendasservico->excluirPorIdVenda($idVenda);
			$cadastrouServicos = $this->vendasservico->cadastrar($servicos);
			$editouVenda = $this->venda->editar($idVenda, $venda);

			if($novoValorRecebido != 0){
				$this->recebimento->cadastrar([
					'id_venda' => $idVenda,
					'valor' => $novoValorRecebido,
					'data' => date('Y-m-d')
				]);
			}

			if ($excluiuVendasServicos && $cadastrouServicos && $editouVenda) {
				retornarJson("Venda alterada com sucesso.", STATUS_SUCESSO);
			} else {
				retornarJson("Algo deu errado. Tente salvar novamente.", STATUS_ERRO);
			}
		} else {
			retornarJson('Verifique se todos os campos foram preenchidos.', STATUS_INVALIDO);
		}
	}

	public function excluir($idVenda = '')
	{
		if (is_numeric($idVenda)) {
			if($this->venda->excluir($idVenda)){
				retornarJson("Venda excluida com sucesso.", STATUS_SUCESSO);
			}else{
				retornarJson('Erro ao excluir venda.', STATUS_ERRO);
			}
		} else {
			retornarJson('Parâmetro inválido.', STATUS_INVALIDO);
		}
	}

	private function vendaValida($venda)
	{
		if ($venda) {

			if (
				!isset($venda['servicos']) ||
				!isset($venda['nome_cliente']) ||
				!isset($venda['valor']) ||
				!isset($venda['id_status_venda'])
			) {
				return false;
			}

			if (
				$venda['nome_cliente'] != '' &&
				$venda['valor'] != '' &&
				$venda['id_status_venda'] != ''
			) {
				return true;
			}
		}
		return false;
	}
}
