<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Relatorio extends CI_Controller
{

	public function __construct(){
		parent::__construct();
		verificarPermissao($this->session->userdata());
		$this->load->model("CustosModel", 'custo');
		$this->load->model("VendasModel", 'venda');
		$this->load->model("RecebimentosModel", 'recebimento');
	}

    public function index()
    {
        $mes = date('m');
        $ano = date('Y');
        $ultimoDia = date("t", mktime(0, 0, 0, $mes, '01', $ano));
        $dadosView = [
            "de" => "$ano-$mes-01",
            "ate" => "$ano-$mes-$ultimoDia",
        ];
        $this->load->view("relatorio/RelatorioView", $dadosView);
    }

    public function buscar($de, $ate)
    {
        if (validarData($de, 'Y-m-d') && validarData($ate, 'Y-m-d')) {
			if(strtotime($de) <= strtotime($ate)){
				$dadosView = [
					'vendido' => $this->buscarVendido($de, $ate),
					'recebido' => $this->buscarRecebido($de, $ate),
					'custos' => $this->buscarCustos($de, $ate)
				];
				$dadosView['lucro'] = $dadosView['vendido'] - $dadosView['custos'];
				$dadosView['pendente'] = $dadosView['vendido'] - $dadosView['recebido'];

				retornarJson("", STATUS_SUCESSO, $dadosView);
			}else{
				retornarJson("A data DE não pode ser maior que a data ATÉ.", STATUS_INVALIDO);
			}
        } else {
            retornarJson("Data inválida.", STATUS_INVALIDO);
        }
	}
	
	private function buscarCustos($de, $ate){
		$custo = $this->custo->somarPorDataLimite($de, $ate);
		if($custo){
			return $custo[0]->valor != ''? $custo[0]->valor: 0;
		}else{
			return 0;
		}
	}

	private function buscarVendido($de, $ate){
		$venda = $this->venda->somarPorDataLimite($de, $ate);
		if($venda){
			return $venda[0]->valor!= ''? $venda[0]->valor: 0;
		}else{
			return 0;
		}
	}

	private function buscarRecebido($de, $ate){
		$recebimento = $this->recebimento->somarPorDataLimite($de, $ate);
		if($recebimento){
			return $recebimento[0]->valor!= ''? $recebimento[0]->valor: 0;
		}else{
			return 0;
		}
	}
}
