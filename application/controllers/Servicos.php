<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicos extends CI_Controller {

	public function __construct(){
		parent::__construct();
		verificarPermissao($this->session->userdata());
		$this->load->model('ServicosModel', 'servico');
	}

	public function index(){
		$this->load->view("servicos/ListaServicosView");
	}

	public function buscarTodos(){
		$servicos = $this->servico->buscarTodos();
		retornarJson('', STATUS_SUCESSO, $servicos);
	}

	public function editar(){
		$idServico = $this->input->post('id_servico');
		$servico = [
			'nome_servico' => $this->input->post('nome_servico')
		];
		if($servico['nome_servico'] == ''){
			retornarJson('Informe o nome do serviço.', STATUS_INVALIDO);
		}else{
			$editou = $this->servico->editar($idServico, $servico);
			if($editou){
				$servico['id_servico'] = $idServico;				
				retornarJson('Serviço alterado com sucesso', STATUS_SUCESSO, $servico);
			}else{
				retornarJson('Erro ao realizar alteração.', STATUS_ERRO);
			}
		}
	}

	public function cadastrar(){
		$servico = [
			'nome_servico' => $this->input->post('nome_servico')
		];
		
		if($servico['nome_servico'] === ''){
			retornarJson('Informe o nome do serviço.', STATUS_INVALIDO);
		}else{
			$id = $this->servico->cadastrar($servico);
			if(is_numeric($id)){
				$servico['id_servico'] = $id;
				retornarJson('Serviço cadastrado com sucesso', STATUS_SUCESSO, $servico);
			}else{
				retornarJson('Erro ao realizar cadastro.', STATUS_ERRO);
			}
		}
	}

	public function excluir($id){
		if($this->servico->podeExcluir($id) === false){
			retornarJson('Este serviço está vinculado a uma venda e não pode ser excluído.', STATUS_ERRO);
			return;
		}
		$excluiu = $this->servico->excluir($id);
		if($excluiu == true){
			retornarJson('Serviço excluído com sucesso.', STATUS_SUCESSO);
		}else{
			retornarJson('Erro ao excluir o serviço.', STATUS_ERRO);
		}		
	}
}