<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Custos extends CI_Controller
{

    public function __construct()
    {
		verificarPermissao($this->session->userdata());
        parent::__construct();
        $this->load->model('CustosModel', 'custo');
    }

    public function index()
    {
        $this->load->view("custos/ListaCustosView", ['data' => date('Y-m-d')]);
	}

	public function paginaCadastrar(){
		$this->load->view('custos/CadastrarCustosView', ['data' => date('Y-m-d')]);
	}
	
	public function paginaEditar($id = ''){
		if(is_numeric($id)){
			$custo = $this->custo->buscarPorId($id);
			if($custo){
				$this->load->view('custos/EditarCustosView', ['custo' => $custo]);
			}else{
				redirect(base_url('Custos'));
			}			
		}else{
			redirect(base_url('Custos'));
		}
	}

	public function buscarPorData($data = '')
    {
        if (validarData($data, 'Y-m-d')) {
            $custos = $this->custo->buscarTodos($data);
            retornarJson('', STATUS_SUCESSO, $custos);
        }else{
			retornarJson('Data inválida.', STATUS_ERRO);
		}

	}
	
	public function editar(){
		$idCusto = $this->input->post('id_custo');
		$custo = [
			'descricao' => $this->input->post('descricao'),
			'valor' => converterValorDB($this->input->post('valor')),
			'data' => $this->input->post('data')
		];
		if($custo['descricao'] == '' || $custo['valor'] == '' || $custo['data'] == ''){
			retornarJson('Preencha todos os campo.', STATUS_INVALIDO);
		}else{
			$editou = $this->custo->editar($idCusto, $custo);
			if($editou){			
				retornarJson('Custo alterado com sucesso', STATUS_SUCESSO);
			}else{
				retornarJson('Erro ao realizar alteração.', STATUS_ERRO);
			}
		}
	}

	public function cadastrar(){
		$custo = [
			'descricao' => $this->input->post('descricao'),
			'valor' => converterValorDB($this->input->post('valor')),
			'data' => $this->input->post('data')
		];
		if($custo['descricao'] == '' || $custo['valor'] == '' || $custo['data'] == ''){
			retornarJson('Preencha todos os campo.', STATUS_INVALIDO);
		}else{
			$cadastrou = $this->custo->cadastrar($custo);
			if($cadastrou){			
				retornarJson('Custo cadastrado com sucesso', STATUS_SUCESSO);
			}else{
				retornarJson('Erro ao realizar cadastro.', STATUS_ERRO);
			}
		}
	}

	public function excluir($id = ''){
		$excluiu = $this->custo->excluir($id);
		if($excluiu == true){
			retornarJson('Custo excluído com sucesso.', STATUS_SUCESSO);
		}else{
			retornarJson('Erro ao excluir o custo.', STATUS_ERRO);
		}		
	}
}
