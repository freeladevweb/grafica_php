<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AgendaServicos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        verificarPermissao($this->session->userdata());
        $this->load->model('VendasModel', 'venda');
        $this->load->model('ServicosModel', 'servico');
        $this->load->model('VendasServicosModel', 'vendasservico');
    }

    public function index()
    {
        $dadosView = [
            'data' => date('Y-m-d'),
            'agendamentos' => $this->venda->buscarAgendadas(date('Y-m-d')),
        ];
        $this->load->view('agenda_servicos/ListaAgendaServicosView', $dadosView);
    }

    public function paginaCadastrar(){
        $dadosView = [
			'servicos' => $this->servico->buscarTodos()
        ];
        
		$this->load->view('agenda_servicos/CadastrarAgendaServicosView', $dadosView);
    }

    public function paginaEditar($id = '')
    {
        if (is_numeric($id)) {
            $dadosView = [
                'servicos' => $this->servico->buscarTodos(),
                'venda' => $this->venda->buscarAgendadoPorId($id)
            ];
            if ($dadosView['venda']) {
                $this->load->view('agenda_servicos/EditarAgendaServicosView', $dadosView);
                return;
            }
        }
        redirect(base_url('AgendaServicos'));
    }

    /**
     * Lista todos as vendas com seus respectivos produtos
     *
     * @param string $data
     * @param integer $pagina
     * @return void
     */
    public function buscarPorData()
    {
        $data = $this->input->post('data');
        $dataValida = validarData($data, 'Y-m-d');

        if ($dataValida) {
            $agendamentos = $this->venda->buscarAgendadas($data);
            $dadosView = [
                'data' => $data,
                'agendamentos' => $agendamentos,
            ];
            $this->load->view('agenda_servicos/ListaAgendaServicosView', $dadosView);
        } else {
            redirect(base_url('AgendaServicos'));
        }
    }

    public function editar()
	{
		$venda = $this->input->post();

		$vendaValida = $this->vendaValida($venda);

		if ($vendaValida) {
			$servicos = $venda['servicos'];
			$idVenda = $venda['id_venda'];
			unset($venda['servicos']);
			unset($venda['id_venda']);
			$venda['valor'] = converterValorDB($venda['valor']);
			$venda['id_status_venda'] = STATUS_VENDA_AGENDAMENTO;

			$excluiuVendasServicos = $this->vendasservico->excluirPorIdVenda($idVenda);
			$cadastrouServicos = $this->vendasservico->cadastrar($servicos);
			$editouVenda = $this->venda->editar($idVenda, $venda);

			if ($excluiuVendasServicos && $cadastrouServicos && $editouVenda) {
				retornarJson("Agendamento alterado com sucesso.", STATUS_SUCESSO);
			} else {
				retornarJson("Algo deu errado. Tente salvar novamente.", STATUS_ERRO);
			}
		} else {
			retornarJson('Verifique se todos os campos foram preenchidos.', STATUS_INVALIDO);
		}
    }

    public function cadastrar(){
        $venda = $this->input->post();

		$vendaValida = $this->vendaValida($venda);

		if ($vendaValida) {
			$servicosSemIdVenda = $venda['servicos'];
			$servicosCadastrar = [];
			unset($venda['servicos']);
			$venda['valor'] = converterValorDB($venda['valor']);
            $venda['data_cadastro'] = date('Y-m-d H:i:s');
            $venda['id_status_venda'] = STATUS_VENDA_AGENDAMENTO;

			$idVenda = $this->venda->cadastrar($venda);

			if (!is_numeric($idVenda)) {
				retornarJson("Algo deu errado. Tente salvar novamente.", STATUS_ERRO);
				return;
			}

			foreach ($servicosSemIdVenda as $servico) {
				$servico['id_venda'] = $idVenda;
				$servicosCadastrar[] = $servico;
			}

			$cadastrouServicos = $this->vendasservico->cadastrar($servicosCadastrar);

			if ($cadastrouServicos) {
				retornarJson("Agendamento cadastrado com sucesso.", STATUS_SUCESSO);
			} else {
				retornarJson("Algo deu errado. Tente salvar novamente.", STATUS_ERRO);
			}
		} else {
			retornarJson('Verifique se todos os campos foram preenchidos.', STATUS_INVALIDO);
		}
    }

    public function excluir($idVenda = '')
	{
		if (is_numeric($idVenda)) {
			if($this->venda->excluir($idVenda)){
				retornarJson("Agendamento excluído com sucesso.", STATUS_SUCESSO);
			}else{
				retornarJson('Erro ao excluir agendamento.', STATUS_ERRO);
			}
		} else {
			retornarJson('Parâmetro inválido.', STATUS_INVALIDO);
		}
    }
    
    public function gerarVenda(){
        $idVenda = $this->input->post('id_venda');
        
        if(is_numeric($idVenda)){
            $venda = [
                'id_status_venda' => STATUS_VENDA_PAGAMENTO_PENDENTE,
                'data_realizacao_servico' => date('Y-m-d'),
            ];
            $editouVenda = $this->venda->editar($idVenda, $venda);
            if($editouVenda){
                retornarJson("Venda gerada com sucesso.", STATUS_SUCESSO);
            }else{
                retornarJson("Erro ao gerar venda.", STATUS_ERRO);
            }
        }else{
            retornarJson('Parâmetro inválido.', STATUS_INVALIDO);
        }
    }
    
    private function vendaValida($venda)
	{
		if ($venda) {

			if (
				!isset($venda['servicos']) ||
				!isset($venda['nome_cliente']) ||
				!isset($venda['valor'])
			) {
				return false;
			}

			if (
				$venda['nome_cliente'] != '' &&
                $venda['valor'] != '' &&
                $venda['data_agendamento'] != ''
			) {
				return true;
			}
		}
		return false;
	}
}
