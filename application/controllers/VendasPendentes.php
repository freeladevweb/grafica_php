<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VendasPendentes extends CI_Controller {
	public function __construct(){
		parent::__construct();
		verificarPermissao($this->session->userdata());
		$this->load->model('VendasModel', 'venda');
	}

	public function index(){
		$this->buscarTodas();
	}

	public function buscarTodas($pagina = 0){
		$this->load->library('pagination');
		$vendas = $this->venda->buscarTodasPendentes($pagina);
        $configPaginacao = configPaginacao('VendasPendentes/buscarTodas/', $this->venda->contarTodasPendente());

        $this->pagination->initialize($configPaginacao);

		$dadosView['paginacao'] = $this->pagination->create_links();
		$dadosView['vendas'] = $vendas;

        $this->load->view("vendas_pendentes/ListaVendasPendentesView", $dadosView);
	}

	public function buscarPorNomeCliente(){
		$cliente = $this->input->get('cliente');
		if($cliente == ''){
			redirect(base_url('VendasPendentes'));
		}

		$vendas = $this->venda->buscarPendentesPorCliente($cliente);
		$dadosView['paginacao'] = "";
		$dadosView['vendas'] = $vendas;
		$dadosView['cliente'] = $cliente;

        $this->load->view("vendas_pendentes/ListaVendasPendentesView", $dadosView);
	}
}