<?php $this->load->view('includes/CabecalhoHTML');?>
<?php $this->load->view('includes/Carregando');?>
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Relatório', 'link' => 'Home']);?>

<div id="conteudo-pagina" class="container">
    <div class="row">
        <div class="col-12">
            <form action="" id="buscar-relatorio">
                <div class="form-row">
                    <div class="form-group col-6">
                        <label for="">De</label>
                        <input type="date" class="form-control" name="de" value="<?=$de?>">
                    </div>
                    <div class="form-group col-6">
                        <label for="">Até</label>
                        <input type="date" class="form-control" name="ate" value="<?=$ate?>">
                    </div>
                    <div class="col col-12">
                        <button class="btn btn-primary btn-block">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table table-dark">
            <tbody>
                <tr>
                    <td><strong>Vendido</strong></td>
                    <td id="vendido">Aguarde</td>
                </tr>
                <tr>
                    <td><strong>Recebido</strong></td>
                    <td id="recebido">Aguarde</td>
                </tr>
                <tr>
                    <td><strong>Recebimento Pendente</strong></td>
                    <td id="pendente">Aguarde</td>
                </tr>
                <tr>
                    <td><strong>Custos</strong></td>
                    <td id="custos">Aguarde</td>
                </tr>
                <tr class="bg-success">
                    <td><strong>Lucro</strong></td>
                    <td id="lucro">Aguarde</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php $this->load->view('includes/Scripts');?>
<script src="<?=base_url(ASSETS . '/js/relatorio/lista_relatorio.js')?>"></script>
<?php $this->load->view('includes/RodapeHTML');?>