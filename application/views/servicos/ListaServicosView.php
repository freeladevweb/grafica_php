<?php $this->load->view('includes/CabecalhoHTML'); ?>
<?php $this->load->view('includes/Carregando');?>
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Serviços', 'link' => 'Home']); ?>

<div id="conteudo-pagina" class="container">
    <div class="table-responsive">
        <table class="table table-striped" id='servicos'>
            <thead>
                <tr>
                    <th>Serviço</th>
                    <th style="width: 140px;">Opções</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
</div>

<button class="btn-redondo bg-primary" id="abrir-modal-cadastrar">+</button>

<?php $this->load->view('servicos/CadastrarEditarServicoView'); ?>
<?php $this->load->view('includes/Scripts'); ?>
<script src="<?= base_url(ASSETS.'/js/servico.js')?>"></script>
<?php $this->load->view('includes/RodapeHTML'); ?>