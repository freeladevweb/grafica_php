<div class="modal fade" id="modal-cadastrar-editar-servico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo-modal-servico">Editar Serviço</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-cadastrar-editar-servico" autocomplete="off">
                <input type="hidden" value="" name="modo-form-servico">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nome-servico">Serviço</label>
                        <input type="text" class="form-control" name="nome_servico">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>