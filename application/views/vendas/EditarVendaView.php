<?php $this->load->view('includes/CabecalhoHTML'); ?>
<?php $this->load->view('includes/Carregando'); ?>
<link rel="stylesheet" href="<?= base_url(ASSETS . '/css/venda.css') ?>">
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Editar Venda', 'link' => $paginaRetorno]); ?>

<div id="conteudo-pagina" class="container">

    <form id="form-editar-venda" autocomplete="off">
        <input type="hidden" name="id-venda" value="<?= $venda[0]->id_venda ?>">
        <input type="hidden" name="status-venda-salvo" value="<?= $venda[0]->id_status_venda ?>">
        <input type="hidden" name="valor-recebido-estatico" value="<?= $venda[0]->valor_recebido ?>">
        <div class="modal-body">
            <div class="row">
                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Cliente</label>
                        <input type="text" class="form-control" name="nome-cliente" id="input-nome-cliente" autofocus value="<?= $venda[0]->nome_cliente ?>" maxlength="80">
                    </div>
                </div>

                <div class="col col-4">
                    <div class="form-group">
                        <label for="nome-servico">Valor</label>
                        <input disabled type="text" class="form-control mascara-dinheiro" name="valor" id="input-valor" value="<?= converterValorUsuario($venda[0]->valor) ?>">
                    </div>
                </div>

                <div class="col col-8">
                    <div class="form-group">
                        <label for="nome-servico">Status</label>
                        <select name="status-venda" class="form-control" id="select-status">
                            <option value="" disabled>Selecione o Status</option>
                            <?php
                            foreach ($statusVenda as $status) :
                                if ($status->id_status_venda != 3) :
                                    $selecionado = $venda[0]->id_status_venda === $status->id_status_venda ? 'selected' : '';
                            ?>
                                    <option value="<?= $status->id_status_venda ?>" <?= $selecionado ?>><?= $status->nome_status_venda ?></option>
                            <?php
                                endif;
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col col-6 modo-pagamento-pendente" <?= $venda[0]->id_status_venda == STATUS_VENDA_PAGO?"style='display:none;'":"" ?>>
                    <div class="form-group">
                        <label for="">Valor Recebido</label>
                        <input type="text" class="form-control mascara-dinheiro" name="valor-recebido">
                    </div>
                </div>

                <div class="col col-6 modo-pagamento-pendente" <?= $venda[0]->id_status_venda == STATUS_VENDA_PAGO?"style='display:none;'":"" ?>>
                    <div class="form-group">
                        <label for="">Valor Restante</label>
                        <input type="text" class="form-control mascara-dinheiro" name="valor-restante" disabled value="<?= converterValorUsuario(($venda[0]->valor - $venda[0]->valor_recebido), false) ?>">
                    </div>
                </div>

                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Descrição</label>
                        <input type="text" class="form-control" name="descricao" id="input-descricao" value="<?= $venda[0]->descricao ?>" maxlength="255">
                    </div>
                </div>
            </div>

            <br>

            <div class="form-row">
                <div class="form-group col-10 col-sm-11">
                    <select name="servicos" class="form-control">
                        <option value="" disabled selected>Selecione o Serviço</option>
                        <?php foreach ($servicos as $servico) : ?>
                            <option value="<?= $servico->id_servico ?>"><?= $servico->nome_servico ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-2 col-sm-1">
                    <a href="#" id="add-servico" class="btn btn-primary btn-block mb-2">+</a>
                </div>
            </div>

            <div class="titulo-tista-servicos">
                Serviços
            </div>
            <div class="container-tbl-servicos-venda">
                <table class="table table-sm" id="tbl-servicos-venda">
                    <tbody>
                        <?php foreach ($venda as $servico) : ?>
                            <tr id-servico="<?= $servico->id_servico ?>">
                                <td><?= $servico->nome_servico ?></td>
                                <td style="text-align: right;">
                                    <a href="#" class="btn btn-danger btn-sm btn-excluir-servico-venda">Excluir</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <a href="<?=base_url($paginaRetorno)?>" class="btn btn-secondary">Cancelar</a>
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </form>
</div>

<?php $this->load->view('includes/Scripts'); ?>
<script src="<?= base_url(ASSETS . '/js/bibliotecas/jquery.mask.min.js') ?>"></script>
<script src="<?= base_url(ASSETS . '/js/mascaras.js') ?>"></script>
<script src="<?= base_url(ASSETS . '/js/venda/cad_editar_venda.js') ?>"></script>
<?php $this->load->view('includes/RodapeHTML'); ?>