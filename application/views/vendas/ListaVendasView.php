<?php $this->load->view('includes/CabecalhoHTML');?>
<?php $this->load->view('includes/Carregando');?>
<link rel="stylesheet" href="<?=base_url(ASSETS . '/css/venda.css')?>">
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Vendas', 'link' => 'Home']);?>

<div id="conteudo-pagina" class="container">
    <div class="row">
        <div class="col-12">
            <form action="" id="buscar-venda-por-data">
                <div class="form-row">
                    <div class="form-group col-9 col-sm-10">
                        <input type="date" id="data-vendas" class="form-control" name="data" value="<?=$data?>">
                    </div>
                    <div class="form-group col-3 col-sm-2">
                        <button type="submit" class="btn btn-primary btn-block mb-2">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table table-hover">
        <tbody class="lista-vendas">

        </tbody>
    </table>

    <div class="col-12 bg-secondary" style="padding-top: 8px; padding-bottom: 8px;">
        <div class="row text-light" id="total"></div>
    </div>
</div>

<a href="<?=base_url('Vendas/paginaCadastrar')?>" class="btn-redondo bg-primary" id="abrir-modal-cadastrar">+</a>
<?php $this->load->view('includes/Scripts');?>
<script src="<?=base_url(ASSETS . '/js/bibliotecas/jquery.mask.min.js')?>"></script>
<script src="<?=base_url(ASSETS . '/js/mascaras.js')?>"></script>
<script src="<?=base_url(ASSETS . '/js/venda/lista_venda.js')?>"></script>
<?php $this->load->view('includes/RodapeHTML');?>