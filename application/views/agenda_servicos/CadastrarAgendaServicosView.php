<?php $this->load->view('includes/CabecalhoHTML'); ?>
<?php $this->load->view('includes/Carregando'); ?>
<link rel="stylesheet" href="<?= base_url(ASSETS . '/css/venda.css') ?>">
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Cadastrar Agendamento', 'link' => 'AgendaServicos']); ?>

<div id="conteudo-pagina" class="container">

    <form id="form-cadastrar-agendamento" autocomplete="off">
        <div class="modal-body">
            <div class="row">
                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Cliente *</label>
                        <input type="text" class="form-control" name="nome-cliente" id="input-nome-cliente" autofocus maxlength="80">
                    </div>
                </div>

                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Celular</label>
                        <input type="text" class="form-control mascara-telefone" name="telefone">
                    </div>
                </div>

                <div class="col col-6">
                    <div class="form-group">
                        <label for="nome-servico">Valor *</label>
                        <input type="text" class="form-control mascara-dinheiro" name="valor" id="input-valor">
                    </div>
                </div>

                <div class="col col-6">
                    <div class="form-group">
                        <label for="nome-servico">Data *</label>
                        <input type="date" class="form-control" name="data-agendamento">
                    </div>
                </div>

                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Descrição</label>
                        <input type="text" class="form-control" name="descricao" id="input-descricao"maxlength="255">
                    </div>
                </div>
            </div>

            <br>

            <div class="form-row">
                <div class="form-group col-10 col-sm-11">
                    <select name="servicos" class="form-control">
                        <option value="" disabled selected>Selecione o Serviço</option>
                        <?php foreach ($servicos as $servico) : ?>
                            <option value="<?= $servico->id_servico ?>"><?= $servico->nome_servico ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-2 col-sm-1">
                    <a href="#" id="add-servico" class="btn btn-primary btn-block mb-2">+</a>
                </div>
            </div>

            <div class="titulo-tista-servicos">
                Serviços *
            </div>
            <div class="container-tbl-servicos-venda">
                <table class="table table-sm" id="tbl-servicos-venda">
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <a href="<?=base_url('AgendaServicos')?>" class="btn btn-secondary">Cancelar</a>
            <button type="submit" class="btn btn-success">Cadastrar</button>
        </div>
    </form>
</div>

<?php $this->load->view('includes/Scripts'); ?>
<script src="<?= base_url(ASSETS . '/js/bibliotecas/jquery.mask.min.js') ?>"></script>
<script src="<?= base_url(ASSETS . '/js/mascaras.js') ?>"></script>
<script src="<?= base_url(ASSETS . '/js/agenda_servico/cad_editar_agenda.js') ?>"></script>
<?php $this->load->view('includes/RodapeHTML'); ?>