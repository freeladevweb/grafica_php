<?php $this->load->view('includes/CabecalhoHTML'); ?>
<?php $this->load->view('includes/Carregando'); ?>
<link rel="stylesheet" href="<?= base_url(ASSETS . '/css/venda.css') ?>">
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Editar Agendamento', 'link' => 'AgendaServicos']); ?>

<div id="conteudo-pagina" class="container">

    <form id="form-editar-agendamento" autocomplete="off">
        <input type="hidden" name="id-venda" value="<?= $venda[0]->id_venda ?>">
        <div class="modal-body">
            <div class="row">
                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Cliente *</label>
                        <input type="text" class="form-control" name="nome-cliente" id="input-nome-cliente" autofocus value="<?= $venda[0]->nome_cliente ?>" maxlength="80">
                    </div>
                </div>

                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Celular</label>
                        <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= $venda[0]->telefone ?>">
                    </div>
                </div>

                <div class="col col-6">
                    <div class="form-group">
                        <label for="nome-servico">Valor *</label>
                        <input type="text" class="form-control mascara-dinheiro" name="valor" id="input-valor" value="<?= converterValorUsuario($venda[0]->valor) ?>">
                    </div>
                </div>

                <div class="col col-6">
                    <div class="form-group">
                        <label for="nome-servico">Data *</label>
                        <input type="date" class="form-control" name="data-agendamento" value="<?= $venda[0]->data_agendamento?>">
                    </div>
                </div>

                <div class="col col-12">
                    <div class="form-group">
                        <label for="nome-servico">Descrição</label>
                        <input type="text" class="form-control" name="descricao" id="input-descricao" value="<?= $venda[0]->descricao ?>" maxlength="255">
                    </div>
                </div>
            </div>

            <br>

            <div class="form-row">
                <div class="form-group col-10 col-sm-11">
                    <select name="servicos" class="form-control">
                        <option value="" disabled selected>Selecione o Serviço</option>
                        <?php foreach ($servicos as $servico) : ?>
                            <option value="<?= $servico->id_servico ?>"><?= $servico->nome_servico ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group col-2 col-sm-1">
                    <a href="#" id="add-servico" class="btn btn-primary btn-block mb-2">+</a>
                </div>
            </div>

            <div class="titulo-tista-servicos">
                Serviços *
            </div>
            <div class="container-tbl-servicos-venda">
                <table class="table table-sm" id="tbl-servicos-venda">
                    <tbody>
                        <?php foreach ($venda as $servico) : ?>
                            <tr id-servico="<?= $servico->id_servico ?>">
                                <td><?= $servico->nome_servico ?></td>
                                <td style="text-align: right;">
                                    <a href="#" class="btn btn-danger btn-sm btn-excluir-servico-venda">Excluir</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <a href="<?=base_url('AgendaServicos')?>" class="btn btn-secondary">Cancelar</a>
            <a href="<?=base_url('AgendaServicos/gerarVenda')?>" id="btn-gerar-venda" class="btn btn-success">Gerar Venda</a>
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </form>
</div>

<?php $this->load->view('includes/Scripts'); ?>
<script src="<?= base_url(ASSETS . '/js/bibliotecas/jquery.mask.min.js') ?>"></script>
<script src="<?= base_url(ASSETS . '/js/mascaras.js') ?>"></script>
<script src="<?= base_url(ASSETS . '/js/agenda_servico/cad_editar_agenda.js') ?>"></script>
<?php $this->load->view('includes/RodapeHTML'); ?>