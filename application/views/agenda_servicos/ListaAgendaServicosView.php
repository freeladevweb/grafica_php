<?php $this->load->view('includes/CabecalhoHTML');?>
<?php $this->load->view('includes/Carregando');?>
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Agenda', 'link' => 'Home']);?>

<div id="conteudo-pagina" class="container">
    <div class="row">
        <div class="col-12">
            <form action="<?=base_url('AgendaServicos/buscarPorData')?>" method="POST">
                <div class="form-row">
                    <div class="form-group col-9 col-sm-10">
                        <input type="date" id="data-vendas" class="form-control" name="data" value="<?=$data?>">
                    </div>
                    <div class="form-group col-3 col-sm-2">
                        <button type="submit" class="btn btn-primary btn-block mb-2">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-hover">
            <theader>
                <tr>
                    <th>Cliente</th>
                    <th style="width: 150px; min=width: 150px; text-align: right">Opções</th>
                </tr>
            </theader>

            <tbody>
                <?php foreach ($agendamentos as $agend): ?>
                <tr>
                    <td><?=$agend->nome_cliente?></td>
                    <td style="width: 130px; text-align: right">
                        <a href="<?=base_url('AgendaServicos/paginaEditar/'.$agend->id_venda)?>" class="btn btn-primary btn-sm">Editar</a>
                        <buttom id-agendamento="<?= $agend->id_venda ?>" class="btn btn-sm btn-danger btn-excluir">Excluir</buttom>
                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>

<a href="<?=base_url('AgendaServicos/paginaCadastrar')?>" class="btn-redondo bg-primary" id="abrir-modal-cadastrar">+</a>

<?php $this->load->view('includes/Scripts');?>
<script src="<?= base_url(ASSETS . '/js/agenda_servico/lista_agenda.js') ?>"></script>
<?php $this->load->view('includes/RodapeHTML');?>