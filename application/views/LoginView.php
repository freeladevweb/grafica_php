<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url(ASSETS.'/css/main.css') ?>">
    <link rel="stylesheet" href="<?= base_url(ASSETS.'/css/tema.css') ?>">
    <link rel="stylesheet" href="<?= base_url(ASSETS.'/css/login.css') ?>">
    <title><?= $this->config->item('titulo_pagina') ?></title>
</head>

<body>

    <?php $this->load->view('includes/Carregando');?>

    <div id="pagina" class="container-fluid">
        <div class="row justify-content-md-center">
            <div class="col-12 col-md-6 col-lg-3">
                <div id="conteudo">
                    <div class="icone">
                        <div class="imagem"></div>
                        <div class="titulo">LOGIN</div>
                    </div>

                    <form id="form-login" method="post" action="<?= base_url('Login/entrar') ?>" autocomplete="off">
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control form-control-lg" name="email" id="email" autofocus>
                        </div>

                        <div class="form-group">
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control form-control-lg" name="senha" id="senha">
                        </div>

                        <div class="form-group">
                            <button class="btn btn-warning btn-lg btn-block">Entrar</button>
                        </div>

                        <div class="form-group">
                            <a href="<?= base_url('EsqueciASenha') ?>">Esqueci a Senha</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="<?= base_url(ASSETS.'/js/main.js') ?>"></script>
    <script src="<?= base_url(ASSETS.'/js/login.js') ?>"></script>
</body>

</html>