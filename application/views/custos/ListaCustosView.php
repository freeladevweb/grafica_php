<?php $this->load->view('includes/CabecalhoHTML');?>
<?php $this->load->view('includes/Carregando');?>
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Custos', 'link' => 'Home']);?>

<div id="conteudo-pagina" class="container">
    <div class="row">
        <div class="col-12">
            <form action="" id="buscar-custo-por-data">
                <div class="form-row">
                    <div class="form-group col-9 col-sm-10">
                        <input type="date" id="data-custo" class="form-control" name="data" value="<?=$data?>">
                    </div>
                    <div class="form-group col-3 col-sm-2">
                        <button type="submit" class="btn btn-primary btn-block mb-2">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-striped" id='custos'>
            <thead>
                <tr>
                    <th>Custo</th>
                    <th style="width: 140px;">Opções</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div class="col-12 bg-secondary" style="padding-top: 8px; padding-bottom: 8px;">
        <div class="row text-light" id="total"></div>
    </div>
</div>

<a href="<?=base_url('Custos/paginaCadastrar')?>" class="btn-redondo bg-primary" id="abrir-modal-cadastrar">+</a>

<?php $this->load->view('includes/Scripts');?>
<script src="<?=base_url(ASSETS . '/js/custo/lista_custo.js')?>"></script>
<?php $this->load->view('includes/RodapeHTML');?>