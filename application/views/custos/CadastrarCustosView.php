<?php $this->load->view('includes/CabecalhoHTML'); ?>
<?php $this->load->view('includes/Carregando');?>
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Novo Custo', 'link' => 'Custos']); ?>

<div id="conteudo-pagina" class="container">
    <form id="form-cadastrar-custo" autocomplete="off">
        <div class="row">
            <div class="col col-12">
                <div class="form-group">
                    <label for="nome-custo">Descrição do Custo</label>
                    <input type="text" class="form-control" name="descricao">
                </div>
            </div>

            <div class="col col-6">
                <div class="form-group">
                    <label for="nome-custo">Valor</label>
                    <input type="text" class="form-control mascara-dinheiro" name="valor">
                </div>
            </div>

            <div class="col col-6">
                <div class="form-group">
                    <label for="nome-custo">Data</label>
                    <input type="date" class="form-control" name="data" value="<?=$data?>">
                </div>
            </div>

            <div class="col-12" style="text-align: right">
                <a href="<?=base_url('Custos')?>" type="reset" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                <button type="submit" class="btn btn-success">Salvar</button>
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('includes/Scripts'); ?>
<script src="<?= base_url(ASSETS . '/js/bibliotecas/jquery.mask.min.js') ?>"></script>
<script src="<?= base_url(ASSETS . '/js/mascaras.js') ?>"></script>
<script src="<?= base_url(ASSETS.'/js/custo/cad_editar_custo.js')?>"></script>
<?php $this->load->view('includes/RodapeHTML'); ?>