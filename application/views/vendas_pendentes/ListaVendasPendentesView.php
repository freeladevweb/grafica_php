<?php $this->load->view('includes/CabecalhoHTML');?>
<?php $this->load->view('includes/Carregando');?>
<?php $this->load->view('includes/MenuVoltar', ['pagina' => 'Vendas Pendentes', 'link' => 'Home']);?>

<div id="conteudo-pagina" class="container">
    <div class="row">
        <div class="col-12">
            <form action="" id="buscar-venda-pedente">
                <div class="form-row">
                    <div class="form-group col-9 col-sm-10">
                        <input type="text" placeholder="Buscar por Cliente" class="form-control" name="cliente" maxlength="80" value="<?=isset($cliente) ? $cliente : ''?>">
                    </div>
                    <div class="form-group col-3 col-sm-2">
                        <button type="submit" class="btn btn-primary btn-block mb-2">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Venda</th>
                    <th style="width: 100px">Editar</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($vendas as $venda): ?>
                <tr>
                    <td>
                        <div><strong>Cliente: </strong><?=$venda->nome_cliente?></div>
                        <div><strong>Data da Venda: </strong><?=converterDataParaUsuario($venda->data_realizacao_servico)?></div>
                        <div><strong>Restante: </strong><span class="text-danger">R$ <?=converterValorUsuario($venda->valor - $venda->valor_recebido)?></span></div>
                    </td>
                    <td>
                        <a href="<?=base_url("Vendas/paginaEditar/" . $venda->id_venda . "/VendasPendentes")?>" class="btn btn-primary btn-sm">Editar</a>
                    </td>
                </tr>
                <?php endforeach;?>

            </tbody>
        </table>
    </div>
    <nav aria-label="Page navigation" class="paginacao" style="display: none">
        <?=$paginacao?>
    </nav>
</div>

<?php $this->load->view('includes/Scripts');?>

<script>
    $(document).ready(function(){
        $(".pagination li").addClass('page-item');
        $(".pagination li a").addClass('page-link');
        $(".paginacao").show();

        $("#buscar-venda-pedente").submit(function(e){
            e.preventDefault();
            let cliente = $("#buscar-venda-pedente input[name='cliente']").val();

            if(cliente === ''){
                window.location.href = baseUrl('VendasPendentes');
            }else{
                if(cliente.length >= 5){
                    window.location.href = baseUrl('VendasPendentes/buscarPorNomeCliente?cliente='+cliente);
                }else{
                    mostrarMsgErro('Necessário informar no mínimo 5 caracteres.');
                }
            }

        });
    });
</script>
<?php $this->load->view('includes/RodapeHTML');?>