<?php

define('ASSETS', 'assets');

define('LIMITE_PAGINACAO', 15);

define('STATUS_VENDA_PAGO', 1);
define('STATUS_VENDA_PAGAMENTO_PENDENTE', 2);
define('STATUS_VENDA_AGENDAMENTO', 3);

define('STATUS_SUCESSO', 'success');
define('STATUS_ERRO', 'error');
define('STATUS_INVALIDO', 'warning');

function retornarJson($mensagem, $status, $dados = [])
{
    echo json_encode([
        'status' => $status,
        'mensagem' => $mensagem,
        'dados' => $dados,
    ]);
}

/**
 * recebe os dados da sessão criada e um array de níveis de 
 * acesso permitidos e verifica se o nível da sessão tem permissão para acessar 
 * o conteúdo da página
 * @param type $sessao
 * @param type $niveis_permitidos
 */
function verificarPermissao($sessao) {
    $acessoPermitido = false;
    if (isset($sessao['logado'])) {
        $acessoPermitido = true;
    }

    if ($acessoPermitido == false) {
        redirect(base_url('Login'));
    }
}


function validarData($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function converterDataParaUsuario($data)
{
    $dataArray = explode('-', $data);
    return $dataArray[2] . "/" . $dataArray[1] . "/" . $dataArray[0];
}

/**
 * Convete o valor monetário com pontos e virgulas
 * para o formato compatível com o banco de dados
 * e retorna esse valor convertido.
 * ----------------------------------------------
 * Ex.: 2.531.154,00 para 2531154.00
 * ----------------------------------------------
 * @param type $valor
 * @return type
 */
function converterValorDB($valor)
{
    $valor = str_replace('.', '', $valor);
    return str_replace(',', '.', $valor);
}

/**
 * Converte o valor para o formato utilizado no BRASIL
 * ex.: 1222.50 para 1.222,00
 * @param type $valor
 * @return type
 */
function converterValorUsuario($valor)
{
    $quantidade = explode('.', $valor);

    if (count($quantidade) > 1) {
        return number_format($valor, 2, ',', '.');
    } else {
        return number_format($valor . '.00', 2, ',', '.');
    }
}

function configPaginacao($url, $totalRegistros)
{
    $config_paginacao = array(
        'base_url' => base_url($url),
        'num_links' => 8,
        'uri_segment' => 3,
        'per_page' => LIMITE_PAGINACAO,
        'total_rows' => $totalRegistros,
        'full_tag_open' => "<ul class='pagination justify-content-center'>",
        'full_tag_close' => '</ul>',
        'first_link' => false,
        'last_link' => false,
        'first_tag_open' => '<li>',
        'first_tag_close' => '</li>',
        'prev_link' => '<span aria-hidden="true"><<</span>',
        'prev_tag_open' => "<li>",
        'prev_tag_close' => "</li>",
        'next_link' => '<span aria-hidden="true">>></span>',
        'next_tag_open' => "<li>",
        'next_tag_close' => "</li>",
        'last_tag_open' => "<li",
        'last_tag_close' => "</li>",
        'cur_tag_open' => "<li class='active'><a href='javascript:void(0)'>",
        'cur_tag_close' => "</a></li>",
        'num_tag_open' => "<li>",
        'num_tag_close' => "</li>",
    );

    return $config_paginacao;
}
