<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Email
 *
 * @author Helder dos Santos
 */
class Email {

    public $ci;

    public function __construct() {
        $this->ci = &get_instance();
    }

    /**
     * Enviar e-mail com EMTP
     * @param type $assunto
     * @param type $mensagem
     * @param type $destinatario
     * @return type
     */
    public function enviar_email($destinatario, $senha) {
        $this->ci->load->library('email');

        $config['protocol'] = 'smtp';
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = 'mail.seedstech.com.br';
        $config['smtp_port'] = '25';
        $config['smtp_user'] = 'cdctecnologia@seedstech.com.br';
        $config['smtp_pass'] = '16E6Zq2018';
        $email = $this->montar_mensagem($senha);

        $this->ci->email->initialize($config);

        $this->ci->email->from('cdctecnologia@seedstech.com.br', 'CDC Tecnologia');
        $this->ci->email->to($destinatario);

        $this->ci->email->subject("Recuperação de senha.");
        $this->ci->email->message($email);


        return $this->ci->email->send();
    }

    private function montar_mensagem($senha) {
       $html = '<div style="width: calc(100% - 40px); background-color: #00795a; padding: 20px; color:#fff">
                        <div style="padding: 10px; text-align: center; font-size: 20px">
                                Sua senha nova senha é: <strong>'.$senha.'</strong>
                        </div>
                 </div>';

        return $html;
    }

}
